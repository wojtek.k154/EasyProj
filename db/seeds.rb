# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rake db:seed (or created alongside the db with db:setup).
#
# Examples:
#
#   cities = City.create([{ name: 'Chicago' }, { name: 'Copenhagen' }])
#   Mayor.create(name: 'Emanuel', city: cities.first)
user = User.new
user.email = '1test@example.com'
user.name = 'Test Test1'
user.password = 'test1234'
user.password_confirmation = 'test1234'
user.avatar = File.new("#{Rails.root}/app/assets/images/avatar/ca.jpg", "r")
user.save!

user1 = User.new
user1.email = '1test1@example.com'
user1.name = 'Test1 Test11'
user1.password = 'test1234'
user1.password_confirmation = 'test1234'
user1.avatar = File.new("#{Rails.root}/app/assets/images/avatar/ba.jpg", "r")
user1.save!

user2 = User.new
user2.email = '1test2@example.com'
user2.name = 'Test2 Test21'
user2.password = 'test1234'
user2.password_confirmation = 'test1234'
user2.avatar = File.new("#{Rails.root}/app/assets/images/avatar/aa.jpg", "r")
user2.save!

user3 = User.new
user3.email = '1test125@example.com'
user3.name = 'Test5 Testd51'
user3.password = 'test1234'
user3.password_confirmation = 'test1234'
user3.avatar = File.new("#{Rails.root}/app/assets/images/avatar/a0.jpg", "r")
user3.save!

p1 = Project.create!(name:"EasyProject", description: "Project allow to creating and organizing project", owner_id: user.uuid, status: "Created")
p2 = Project.create!(name:"NewProjec", description: "Another description", owner_id: user1.uuid, status: "Created")
p3 = Project.create!(name:"Softprojecq", description: "Krest ", owner_id: user2.uuid, status: "Created")
p4 = Project.create!(name:"NextProject", description:  "and organizing project", owner_id: user3.uuid, status: "Created")

p1.user_projects.create!(user_id: user.uuid)
p2.user_projects.create!(user_id: user1.uuid)
p3.user_projects.create!(user_id: user2.uuid)
p4.user_projects.create!(user_id: user3.uuid)

Sprint.create!(name: "First Sprint", start_date: Date.today, end_date: Date.today + 1.week, project: p1, status: "Created")
Sprint.create!(name: "First Sprint", start_date: Date.today, end_date: Date.today + 1.week, project: p2, status: "Created")
Sprint.create!(name: "First Sprint", start_date: Date.today, end_date: Date.today + 1.week, project: p3, status: "Created")
Sprint.create!(name: "First Sprint", start_date: Date.today, end_date: Date.today + 1.week, project: p4, status: "Created")

Task.create!(name: "First Task", description: "description First Task Fifith", priority: 1, kind: "Task", project: p1, status: "Created")
Task.create!(name: "First Task", description: "description First Task Fifith", priority: 1, kind: "Task", project: p4, status: "Created")
Task.create!(name: "First Task", description: "description First Task Fifith", priority: 1, kind: "Task", project: p3, status: "Created")
Task.create!(name: "First Task", description: "description First Task Fifith", priority: 1, kind: "Task", project: p2, status: "Created")

Task.create!(name: "Second Task", description: "description Second TaskF ifith", priority: 1, kind: "Task", project: p1, status: "Created")
Task.create!(name: "Second Task", description: "description Second TaskF ifith", priority: 1, kind: "Task", project: p4, status: "Created")
Task.create!(name: "Second Task", description: "description Second TaskF ifith", priority: 1, kind: "Task", project: p3, status: "Created")
Task.create!(name: "Second Task", description: "description Second TaskF ifith", priority: 1, kind: "Task", project: p2, status: "Created")

Task.create!(name: "Third Task", description: "description Third Second Fifith Task", priority: 2, kind: "SubTask", project: p1, status: "Created")
Task.create!(name: "Third Task", description: "description Third Second Fifith Task", priority: 2, kind: "SubTask", project: p4, status: "Created")
Task.create!(name: "Third Task", description: "description Third Second Fifith Task", priority: 2, kind: "SubTask", project: p3, status: "Created")
Task.create!(name: "Third Task", description: "description Third Second Fifith Task", priority: 2, kind: "SubTask", project: p2, status: "Created")

Task.create!(name: " Fourtj Task" ,description: "description Fourtj Task Fifith", priority: 2, kind: "Task", project: p1, status: "Created")
Task.create!(name: " Fourtj Task", description: "description Fourtj Task Fifith", priority: 2, kind: "Task", project: p4, status: "Created")
Task.create!(name: " Fourtj Task", description: "description Fourtj Task Fifith", priority: 2, kind: "Task", project: p3, status: "Created")
Task.create!(name: " Fourtj Task", description: "description Fourtj Task Fifith", priority: 2, kind: "Task", project: p2, status: "Created")

Task.create!(name: "Fifith Task", description: "description Fifith Fifith Task", priority: 3, kind: "SubTask", project: p1, status: "Created")
Task.create!(name: "Fifith Task", description: "description Fifith Fifith Task", priority: 3, kind: "SubTask", project: p4, status: "Created")
Task.create!(name: "Fifith Task", description: "description Fifith Fifith Task", priority: 3, kind: "SubTask", project: p3, status: "Created")
Task.create!(name: "Fifith Task", description: "description Fifith Fifith Task", priority: 3, kind: "SubTask", project: p2, status: "Created")
