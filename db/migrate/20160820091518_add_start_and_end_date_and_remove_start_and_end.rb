class AddStartAndEndDateAndRemoveStartAndEnd < ActiveRecord::Migration
  def change
    remove_column :tasks, :start
    add_column :tasks, :begin, :date
  end
end
