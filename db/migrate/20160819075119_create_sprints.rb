class CreateSprints < ActiveRecord::Migration
  def change
    create_table :sprints do |t|
      t.belongs_to :project
      t.string :name
      t.date :start_date
      t.date :end_date
      t.string :status
      t.timestamps null: false
    end
  end
end
