class CreateNotifications < ActiveRecord::Migration
  def change
    create_table :notifications do |t|
      t.integer :user_id
      t.string :trackable_type
      t.integer :trackable_id
      t.string :key
      t.timestamps null: false
    end
    add_index :notifications, [:trackable_type, :trackable_id]
  end
end
