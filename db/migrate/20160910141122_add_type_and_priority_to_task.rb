class AddTypeAndPriorityToTask < ActiveRecord::Migration
  def change
    add_column :tasks, :kind, :string
    add_column :tasks, :priority, :integer
  end
end
