class AddUidToUser < ActiveRecord::Migration
  def change
    add_column :users, :uuid, :uuid, default: 'uuid_generate_v4()'
    remove_column :projects, :owner_id
    remove_column :tasks, :user_id
    remove_column :user_projects, :user_id
    remove_column :notifications, :user_id
  end
end
