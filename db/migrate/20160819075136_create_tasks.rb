class CreateTasks < ActiveRecord::Migration
  def change
    create_table :tasks do |t|
      t.belongs_to :sprint
      t.belongs_to :project
      t.belongs_to :user
      t.string :name
      t.text :description
      t.string :status
      t.string :estimation
      t.string :remaining
      t.string :worked
      t.date :start
      t.date :finish
      t.timestamps null: false
    end
  end
end
