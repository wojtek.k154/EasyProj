class AddUserUidToResources < ActiveRecord::Migration
  def change
    add_column :projects, :owner_id, :uuid
    add_column :tasks, :user_id, :uuid
    add_column :user_projects, :user_id, :uuid
    add_column :notifications, :user_id, :uuid
  end
end
