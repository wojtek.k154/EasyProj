class User < ActiveRecord::Base
  # Include default devise modules. Others available are:
  # , :confirmable, :lockable, :timeoutable and :omniauthable
  self.primary_key = 'uuid'

  devise :database_authenticatable, :registerable,
         :recoverable, :rememberable, :trackable, :validatable

  has_many :notifications, dependent: :destroy
  has_many :owner_projects, foreign_key: 'owner_id', class_name: 'Project'
  has_many :user_projects, dependent: :destroy
  has_many :projects, through: :user_projects

  has_many :tasks
  
  has_attached_file :avatar, styles: {original: "300x300>"}, #:default_url => "missing.png",
    storage: :dropbox,
    dropbox_credentials: Rails.root.join("config/dropbox.yml"),
    dropbox_visibility: 'public',
    dropbox_options: {
        environment: ENV["RACK_ENV"],
        path: proc { |style| "#{style}/#{uuid}_#{avatar.original_filename}"}
      }

  validates_attachment_content_type :avatar, content_type: /\Aimage\/.*\Z/
  validates :email, :name, presence: true
 end
