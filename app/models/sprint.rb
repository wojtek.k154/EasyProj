class Sprint < ActiveRecord::Base
  has_many :notifications, as: :trackable, dependent: :destroy
  belongs_to :project
  has_many :tasks
  validates :project_id, :name, :start_date, :end_date, :status, presence: true

end
