class Notification < ActiveRecord::Base
  belongs_to :trackable, polymorphic: true
  belongs_to :user
  validates :user_id, :trackable_type, :trackable_id, presence: true
end
