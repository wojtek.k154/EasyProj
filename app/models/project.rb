class Project < ActiveRecord::Base
  attr_accessor :newuser, :remove_user_project, :projects
  has_many :user_projects, dependent: :destroy
  has_many :users, through: :user_projects
  has_many :notifications, as: :trackable, dependent: :destroy
  belongs_to :owner, class_name: 'User', foreign_key: 'owner_id'
  has_many :sprints, dependent: :destroy
  has_many :tasks, dependent: :destroy
  def newuser=(val)
    self.user_projects.where(user_id: val).first_or_create
  end

  def newuser
  end

  def remove_user_project
  end

  def remove_user_project=(val)
    self.user_projects.find_by(user_id: val).destroy
  end

  validates :name, :description, :status, :owner_id, presence: true
end
