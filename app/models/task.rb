class Task < ActiveRecord::Base
  has_many :notifications, as: :trackable, dependent: :destroy
  belongs_to :project
  belongs_to :sprint
  belongs_to :user

  validates :name, :description, :kind, :priority, presence: true
  validates :estimation, :remaining, :worked, format: { with: /\A[0-9]{,3}[h]{1}+[0-9]{,2}[m]{1}|[0-9]{,3}[hm]{1}$|^\Z/i, :multiline => true, message: "only allows letters" }

end
