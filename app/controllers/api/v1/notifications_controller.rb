class Api::V1::NotificationsController < Api::ApiController
  def index
    @notifications = Notification.all 
    render json: @notifications
  end

  def create
    @notification = Notification.new(notifications_params)
    if @notification.save
      render json: @notification
    else
      render json: @notification.errors, status: :unprocessable_entity
    end
  end

  private
    def notifications_params
      params.permit(:key, :trackable_id, :trackable_type, :user_id)
    end
end
