class Api::V1::UsersController < Api::ApiController
  before_action :set_user, only: [:show]
  skip_before_filter :verify_authenticity_token
  def index
    if params[:projectID]
      @project = current_user.projects.find(params[:projectID])
      @users = @project.users
    else
      @users = User.all
    end
    render json: @users
  end

  def show
    render json: @user
  end

  def update
    if current_user.update_attributes(user_params)
      render json: current_user
    else
      render json: current_user.errors, status: :unprocessable_entity
    end
  end

  private
    def set_user
      @user = User.find(params[:uuid])
    end

    def user_params
      params.require(:user).permit(:email, :name, :avatar, :uuid)
    end
end
