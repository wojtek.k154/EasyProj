class Api::V1::TasksController < Api::ApiController
  before_action :set_task, only: [:show, :update, :destroy]
  skip_before_action :verify_authenticity_token
  def index
    @project = current_user.projects.find(params[:project_id])
    @tasks = @project.tasks.order('created_at DESC')
    render json: @tasks
  end

  def show
    render json: @task
  end

  def create
    @project = current_user.projects.find(params[:project_id])
    @task = @project.tasks.new(task_params)
    if @task.save
      render json: @task
    else
      render json: @task.errors, status: :unprocessable_entity
    end
  end

  def update 
    if @task.update_attributes(task_params)
      render json: @task
    else
      render json: @task.errors, status: :unprocessable_entity
    end
  end

  def destroy
    @task.destroy
    render json: @task
  end

  private
    def set_task
      @project = current_user.projects.find(params[:project_id])
      @task = @project.tasks.find(params[:id])
    end

    def task_params
      params.require(:task).permit(:id, :sprint_id, :project_id, :user_id, :name, :description, :status, :estimation, :remaining, :worked, :begin, :finish, :kind, :priority, :created_at, :updated_at)
    end
end
