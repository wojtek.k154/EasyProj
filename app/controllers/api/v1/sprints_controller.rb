class Api::V1::SprintsController < Api::ApiController
  before_action :set_sprint, only: [:show, :update, :destroy]
  def index
    @project = current_user.projects.find(params[:project_id])
    @sprints = @project.sprints.order('created_at DESC')
    render json: @sprints
  end

  def show
    render json: @sprint
  end

  def create
    @project = current_user.projects.find(params[:project_id])
    @sprint = @project.sprints.new(sprint_params)
    if @sprint.save
      render json: @sprint
    else
      render json: @sprint.errors, status: :unprocessable_entity
    end
  end

  def update
    if @sprint.update(sprint_params)
      render json: @sprint
    else
      render json: @sprint.errors, status: :unprocessable_entity
    end
  end

  def destroy
    @sprint.destroy
    render json: @sprint
  end

  private
    def set_sprint
      @project = current_user.projects.find(params[:project_id])
      @sprint = @project.sprints.find(params[:id])
    end

    def sprint_params
      params.permit(:id, :project_id, :name, :start_date, :end_date, :status)
    end
end
