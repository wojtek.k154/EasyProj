class Api::V1::ProjectsController < Api::ApiController
  before_action :set_project, only: [:show, :update, :destroy]
  def index
    @projects = current_user.projects.order("created_at DESC")
    render json: @projects
  end

  def show
    render json: @project
  end

  def create
    @project = current_user.projects.new(project_params)
    if @project.save
      @project.update(newuser: current_user.uuid)
      render json: @project
    else
      render json: @project.errors, status: :unprocessable_entity
    end
  end

  def update
    if @project.update_attributes(project_params)
      render json: @project
    else
      render json: @project.errors, status: :unprocessable_entity
    end
  end

  def destroy
    @project.destroy
    render json: @project
  end

  private

  def set_project
    @project = Project.includes(:tasks, :sprints, :users).find(params[:id])
  end

  def project_params
    params.permit(:name, :description, :status, :owner_id, :newuser, :remove_user_project)
  end
end
