var app1 = angular.module('Easypro', [
																			'ngAnimate',
																			'ui.router',
																			'templates',
																			'ngResource',
																			'dndLists',
																			'Devise',
																			'ngFileUpload',
																			'ui.bootstrap',
																			'highcharts-ng'
																		]);

app1.run(function($compile, $rootScope, $document) {
	return $document.on('page:load', function(){
		var body, finish;
		body = angular.element('body');
		finish = $compile(body.html())($rootScope);
		return body.html(finish);
	});
});

app1.config(function(AuthProvider, AuthInterceptProvider) {
	//AuthProvider.loginMethod('POST');
  //AuthProvider.loginPath('/users/sign_in.json');

	AuthProvider.logoutPath('/users/sign_out.json');
  AuthProvider.logoutMethod('DELETE');

  AuthProvider.registerPath('/users.json');
  AuthProvider.registerMethod('POST');

  AuthProvider.sendResetPasswordInstructionsPath('/users/password.json');
  AuthProvider.sendResetPasswordInstructionsMethod('POST');

	AuthProvider.resetPasswordPath('/users/password.json');
  AuthProvider.resetPasswordMethod('PUT');

  AuthInterceptProvider.interceptAuth(true);
});

app1.config(function($logProvider){
    $logProvider.debugEnabled(true);
});
app1.config(function ($stateProvider, $urlRouterProvider, $locationProvider) {
   /**
   * Routes and States
   */
  $stateProvider
			.state('nav', {
				url: '/dashboard',
				templateUrl: 'api/v1/nav/navigation.html',
				controller: 'NavCtrl'
			})
      .state('nav.projects', {
        url: '/projects',
        templateUrl: 'api/v1/projects/projects.html',
        controller: 'ProjectsCtrl'
      })
      .state('nav.project',{
        url: '/project/:project_id',
        templateUrl: 'api/v1/projects/project.html',
        controller: 'ProjectCtrl'
      })
			// Project nested pages
				.state('nav.project.charts',{
					url: '/charts',
					templateUrl: 'api/v1/projects/charts.html',
					controller: 'ChartCtrl'
				})
				.state('nav.project.tasks', {
					url: "/tasks",
					templateUrl: "api/v1/tasks/project_tasks.html",
					controller: "ProjectTasksCtrl"
				})
				.state('nav.project.sprints', {
					url: '/sprints',
					templateUrl: 'api/v1/sprints/sprints.html',
					controller: 'SprintsCtrl'
				})
				.state('nav.project.users', {
					url: '/users',
					templateUrl:'api/v1/users/users.html',
					controller: 'UsersCtrl'
				})
			//project separate pages
			.state('nav.sprint', {
				url: '/project/:project_id/sprint/:sprint_id',
				templateUrl: 'api/v1/sprints/sprint.html',
				controller: 'SprintCtrl'
			})
			.state('nav.wrorkingtasks', {
				url: '/project/:project_id/sprint/:sprint_id/tasks',
				templateUrl:'api/v1/tasks/work_tasks.html',
				controller: 'TasksCtrl'
			})
			.state('nav.notifications', {
				url:'/notification',
				templateUrl: 'api/v1/notifications/notifications.html',
				controller: 'NotificationCtrl'
			})

			.state('nav.task', {
				url: '/project/:project_id/tasks/:task_id',
				templateUrl:'api/v1/tasks/task.html',
				controller: 'TaskCtrl'
			})
			.state('login', {
				url: '/login',
				templateUrl:'api/v1/auth/login.html',
				controller: 'AuthCtrl'
			})
			.state('signup', {
				url: '/signup',
				templateUrl:'api/v1/auth/signup.html',
				controller: 'AuthCtrl'
			})
			.state('nav.user',{
				url: '/user',
				templateUrl:'api/v1/users/user.html',
				controller: 'UserCtrl'
			})
			.state('resetpassword', {
				url: '/resetpassword/:reset_token',
				templateUrl:'api/v1/auth/resetpassword.html',
				controller: 'AuthCtrl'
			})
			.state('sendinstructions', {
				url: '/sendinstructions',
				templateUrl:'api/v1/auth/sendinstructions.html',
				controller: 'AuthCtrl'
			})
			.state('startpage', {
				url: '/',
				templateUrl: 'api/v1/auth/startpage.html',
				controller: 'AuthCtrl'
			})
			.state('nav.updateuser',{
				url: '/users/edit',
				templateUrl: 'api/v1/users/update_user.html',
				controller: 'UserUpdateCtrl'
			});
  // default fall back route
	$urlRouterProvider.otherwise('/dashboard/projects');

  // enable HTML5 Mode for SEO
  $locationProvider.html5Mode({
    enabled: true,
    requireBase: false
  });
});
