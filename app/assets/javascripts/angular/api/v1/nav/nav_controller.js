app1.controller("NavCtrl", ['$scope', '$state','Auth', function($scope,$state, Auth){
  Auth.currentUser().then(function(user) {
    $scope.user = user;
    gon.current_user = user;
    }, function(error) {
      $state.go('startpage');
  });
  $scope.isLoggedIn = function(){
    console.log(Auth.isAuthenticated());
      //$state.go('nav.projects');
  };

  $scope.isLoggedIn();
  $scope.logout = function(){
    Auth.logout($scope.user).then(function(oldUser) {
      $state.go('login');
      toastr.success('You logged out from application!', 'Success!');
    }, function(error) {
      toastr.error('Error while logging out!', 'Faliure!');
    });
  };
  $scope.$on('devise:new-registration', function (e, user){
    $scope.user = user;
  });
  $scope.$on('devise:login', function (e, user){
    $scope.user = user;
  });
  $scope.$on('devise:logout', function (e, user){
    $scope.user = {};
    $state.go('login');
  });
  $scope.$on('devise:send-reset-password-instructions-successfully', function(event) {
      $state.go('login');
  });
  $scope.$on('devise:reset-password-successfully', function(event) {
    $state.go('login');
  });
}]);
