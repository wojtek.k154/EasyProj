app1.directive('taskform', function(){
  return {
    restrict: 'E',
    controllerAs: 'ProjectTasksCtrl',
    templateUrl: 'api/v1/tasks/directives/task-form.html'
  };
});

app1.directive('taskitem', function(){
  return {
    restrict: 'E',
    controllerAs: 'ProjectTasksCtrl',
    templateUrl: 'api/v1/tasks/directives/task-item.html'
  };
});
app1.directive('projectheaderone', function() {
  return {
    restrict: 'E',
    controllerAs: 'SprintsCtrl',
    templateUrl: 'api/v1/sprints/directives/projectheader.html'
  };
});
