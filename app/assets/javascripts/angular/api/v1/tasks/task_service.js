app1.factory('Task', ['$resource', function ($resource) {
    return $resource("/api/v1/projects/:projectID/tasks/:taskID", {projectID: "@project_id", taskID: "@id"},{
          update: {method: 'PUT' },
          get: { method: 'GET', isArray: false }
    });
}]);
