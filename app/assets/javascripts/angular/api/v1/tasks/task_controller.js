app1.controller("TasksCtrl", ['$scope', 'Project', 'User', 'Sprint', 'Task', 'Notification', '$stateParams', 'Auth',
  function($scope, Project, User, Sprint, Task, Notification, $stateParams, Auth) {
    $scope.loading = true;
    Auth.currentUser().then(function(user) {
      gon.current_user = user;
      }, function(error) {
        $state.go('startpage')
    });
    Sprint.get({
      projectID: $stateParams.project_id,
      sprintID: $stateParams.sprint_id
    }).$promise.then(
      function(success) {
        $scope.sprint = success;
      }
    );
    Project.get({
      projectID: $stateParams.project_id
    }).$promise.then(
      function(success) {
        $scope.project = success;
      }
    );
    $scope.p = $stateParams;
    $scope.timesAmount = {
      estimation: 0,
      worked: 0,
      remaining: 0
    };
    $scope.lists = {
      sumaryEstimate: '',
      todo: {
        allowed: [
           
        ],
        name: 'To do tasks',
        items: []
      },
      actual: {
        name: 'In Progress tasks',
        allowed: [
          "Created"
        ],
        items: []
      },
      closed: {
        name: 'Closed tasks',
        allowed: [
          "InProgres"
        ],
        items: []
      }
    };
    Task.query({
      projectID: $stateParams.project_id
    }).$promise.then(
      function(success) {
        for (var i = 0; i < success.length; i++) {
          if (success[i].sprint_id == $stateParams.sprint_id) {
            if (success[i].user_id == gon.current_user.uuid || success[i].user_id == null) {
              if (success[i].status == 'Created') {
                $scope.lists.todo.items.push(success[i]);
              } else if (success[i].status == 'InProgres') {
                $scope.lists.actual.items.push(success[i]);
              } else {
                $scope.lists.closed.items.push(success[i]);
              }
            }
          }
        }
        $scope.timesAggerate();
      }
    ).finally(function() {
      $scope.loading = false;
    });

    $scope.reopenTask = function(item, index) {
      Task.update({
        projectID: $stateParams.project_id,
        taskID: item.id
      }, {
        status: 'Created'
      }).$promise.then(
        function(success) {
          toastr.success('Task Reopened!', 'Success!');
          Notification.save({
            key: ' reopened task ',
            trackable_id: success.id,
            trackable_type: 'Task',
            user_id: gon.current_user.uuid
          });
          $scope.lists.closed.items.splice(index, 1);
          $scope.lists.todo.items.unshift(success);
          $scope.timesAggerate();
        },
        function(error) {
          $scope.errors = error;
          toastr.error('Occured error!', 'Faliure!');
        }
      );
    };

    $scope.timesAggerate = function() {
      var task = {
        est: 0,
        wor: 0,
        rem: 0
      };

      for (var i = 0; i < $scope.lists.todo.items.length; i++) {
        if ($scope.lists.todo.items[i].estimation) {
          task.est += Duration.parse($scope.lists.todo.items[i].estimation);
        }
        if ($scope.lists.todo.items[i].worked) {
          task.wor += Duration.parse($scope.lists.todo.items[i].worked);
        }
        if ($scope.lists.todo.items[i].remaining) {
          task.rem += Duration.parse($scope.lists.todo.items[i].remaining);
        }
      }

      for (var j = 0; j < $scope.lists.actual.items.length; j++) {
        if ($scope.lists.actual.items[j].estimation) {
          task.est += Duration.parse($scope.lists.actual.items[j].estimation);
        }
        if ($scope.lists.actual.items[j].worked) {
          task.wor += Duration.parse($scope.lists.actual.items[j].worked);
        }
        if ($scope.lists.actual.items[j].remaining) {
          task.rem += Duration.parse($scope.lists.actual.items[j].remaining);
        }
      }

      for (var k = 0; k < $scope.lists.closed.items.length; k++) {
        if ($scope.lists.closed.items[k].estimation) {
          task.est += Duration.parse($scope.lists.closed.items[k].estimation);
        }
        if ($scope.lists.closed.items[k].worked) {
          task.wor += Duration.parse($scope.lists.closed.items[k].worked);
        }
        if ($scope.lists.closed.items[k].remaining) {
          task.rem += Duration.parse($scope.lists.closed.items[k].remaining);
        }
      }
      $scope.timesAmount.estimation = new Duration(task.est).toString();
      $scope.timesAmount.worked = new Duration(task.wor).toString();
      $scope.timesAmount.remaining = new Duration(task.rem).toString();

    };
    $scope.setInProgres = function(item, index) {
      Task.update({
        projectID: $stateParams.project_id,
        taskID: item.id
      }, {
        status: 'InProgres'
      }).$promise.then(
        function(success) {
          toastr.success('Task status set as InProgres!', 'Success!');
          if (success.user_id == null) {
            Task.update({
              projectID: $stateParams.project_id,
              taskID: item.id
            }, {
              user_id: gon.current_user.uuid
            });
          }
          Notification.save({
            key: 'set status InProgres in task',
            trackable_id: success.id,
            trackable_type: 'Task',
            user_id: gon.current_user.id
          });
          $scope.lists.todo.items.splice(index, 1);
          for (var i = 0; i < $scope.lists.actual.items.length; i++) {
            if ($scope.lists.actual.items[i].status == 'Created') {
              $scope.lists.actual.items[i] = success;
            }
          }
          $scope.timesAggerate();
        },
        function(error) {
          $scope.errors = error;
          toastr.error('Occured error!', 'Faliure!');
        }
      );

    };
    $scope.setClosed = function(item, index) {
      Task.update({
        projectID: $stateParams.project_id,
        taskID: item.id
      }, {
        status: 'Closed'
      }).$promise.then(
        function(success) {
          $scope.lists.actual.items.splice(index, 1);
          toastr.success('Task status set as Closed!', 'Success!');
          Notification.save({
            key: 'set status Closed in task',
            trackable_id: success.id,
            trackable_type: 'Task',
            user_id: gon.current_user.uuid
          });
          for (var i = 0; i < $scope.lists.closed.items.length; i++) {
            if ($scope.lists.closed.items[i].status == 'InProgres') {
              $scope.lists.closed.items[i] = success;
            }
          }
          $scope.timesAggerate();
        },
        function(error) {
          $scope.errors = error;
          toastr.error('Faliure!', 'Occured error!');
        }
      );

    };

  }
]);

app1.controller("ProjectTasksCtrl", ['$scope', 'Project', 'User', 'Sprint', 'Task', 'Notification', '$stateParams', 'Auth',
  function($scope, Project, User, Sprint, Task, Notification, $stateParams, Auth) {
    $scope.loading = true;
    Auth.currentUser().then(function(user) {
      gon.current_user = user;
      }, function(error) {
        $state.go('startpage')
    });
    Project.get({
      projectID: $stateParams.project_id
    }).$promise.then(
      function(success) {
        $scope.project = success;
        $scope.users = $scope.project.users;
        for (var i = 0; i < $scope.users.length; i++) {
          if ($scope.users[i].uuid == gon.current_user.uuid) {
            gon.current_user = $scope.users[i];
            $scope.users.splice(i, 1);
          }
        }
        $scope.tasks = $scope.project.tasks;
        $scope.projectTaskTime();
        $scope.sprints = $scope.project.sprints;
      }
    ).finally(function() {
      $scope.loading = false;
    });



    $scope.tempUser = {
      name: null,
      email: null
    };
    $scope.projectTaskTime = function() {
      var wor = 0;
      var est = 0;
      for (var i = 0; i < $scope.tasks.length; i++) {
        if ($scope.tasks[i].worked) {
          wor += Duration.parse($scope.tasks[i].worked);
        }
        if ($scope.tasks[i].estimation) {
          est += Duration.parse($scope.tasks[i].estimation);
        }
      }

      $scope.est = new Duration(est).toString();
      $scope.wor = new Duration(wor).toString();
    };
    $scope.newTask = {
      status: "Created"
    };
    $scope.setTaskNew = function() {
      $scope.newTask = {
        status: "Created",
        worked: "0h"
      };
    };


    $scope.assignUserToTask = function(p) {
      if (typeof p == "undefined") {
        $scope.newTask.user_id = gon.current_user.uuid;
        $scope.tempUser = gon.current_user;
      } else {
        $scope.newTask.user_id = p.uuid;
        $scope.tempUser = p;
      }
    };

    $scope.setUpdateTask = function(wp, i) {
      $scope.i = i;
      $scope.newTask = wp;
      if (wp.begin) {
        $scope.newTask.begin = new Date(wp.begin);
      }
      if (wp.finish) {
        $scope.newTask.finish = new Date(wp.finish);
      }
    };
    $scope.addTask = function() {
      $scope.newTask.remaining = $scope.newTask.estimation;
      $scope.newTask.worked = "0h";
      Task.save({
        projectID: $stateParams.project_id
      }, $scope.newTask).$promise.then(
        function(success) {
          toastr.success('Success!', 'New task added!');
          Notification.save({
            key: 'added new task',
            trackable_id: success.id,
            trackable_type: 'Task',
            user_id: gon.current_user.uuid
          });
          $scope.tasks.unshift(success);
          $scope.taskCreate = true;
          $scope.newTask = {
            status: "Created"
          };
          $scope.tempUser = {
            name: null,
            email: null
          };
          $scope.projectTaskTime();
        },
        function(error) {
          $scope.errors = error;
          toastr.error('Occured error!', 'Faliure!');
        }
      )
    };

    $scope.taskUpdate = function() {
      Task.update({
        projectID: $stateParams.project_id,
        sprintID: $scope.newTask.id
      }, $scope.newTask).$promise.then(
        function(success) {
          toastr.success('Task updated!', 'Success!');
          Notification.save({
            key: 'updated task',
            trackable_id: success.id,
            trackable_type: 'Task',
            user_id: gon.current_user.uuid
          });
          $scope.tasks[$scope.i] = success;
          $scope.tUpdate = true;
          $scope.i = null;
          $scope.newTask = {
            status: "Created"
          };
          $scope.projectTaskTime();
        },
        function(error) {
          Task.get({
            projectID: $stateParams.project_id,
            taskID: $scope.newTask.id
          }).$promise.then(
            function(success) {
              $scope.tasks[$scope.i] = success;
            }
          );
          $scope.errors = error;
          $scope.newTask = {
            status: "Created"
          };
          toastr.error('Occured error!', 'Faliure!');
        }
      );
    };
    $scope.deleteTask = function(Id, index) {
      Task.delete({
        projectID: $stateParams.project_id,
        taskID: Id
      }).$promise.then(
        function(success) {
          toastr.success('Success!', 'Task deleted!');
          $scope.tasks.splice(index, 1);
          $scope.projectTaskTime();
        },
        function(error) {
          toastr.error('Occured error!', 'Faliure!');
          $scope.tasks = Task.query({
            projectID: $stateParams.project_id
          });
        }
      );
    };

    $scope.setKind = function(val){
      $scope.newTask.kind = val;
    };
    $scope.setPriority = function(val){
      $scope.newTask.priority = val;
    };
  }
]);

app1.controller("TaskCtrl", ['$scope', 'Project', 'User', 'Sprint', 'Task', 'Notification', '$stateParams', 'Auth',
  function($scope, Project, User, Sprint, Task, Notification, $stateParams, Auth) {
    $scope.loading = true;
    $scope.task = {};
    $scope.project = {};
    $scope.updateTask = {};
    $scope.notifications = [];
    Auth.currentUser().then(function(user) {
      gon.current_user = user;
      }, function(error) {
        $state.go('startpage')
    });
    Task.get({
      projectID: $stateParams.project_id,
      taskID: $stateParams.task_id
    }).$promise.then(
      function(success) {
        $scope.task = success;
        $scope.updateTask = $scope.task;
      }
    ).finally(function() {
      $scope.loading = false;
    });

    Project.get({
      projectID: $stateParams.project_id
    }).$promise.then(
      function(success) {
        $scope.project = success;
      }
    );

    Notification.query().$promise.then(
      function(success) {
        for (var i = 0; i < success.length; i++) {
          if (success[i].trackable_type == 'Task' && success[i].trackable_id == $stateParams.task_id) {
            $scope.notifications.push(success[i]);

          }
        }
      },
      function(error) {

      }
    );

    $scope.statusUpadte = function(){
      Task.update({
        projectID: $stateParams.project_id,
        taskID: $scope.task.id
      }, {status: $scope.updateTask.status}).$promise.then(
        function(success) {
          $scope.task = success;
          toastr.success('Task status updated!', 'Success!');
          $scope.updateTask.worked = null;
          Notification.save({
            key: 'set status in task',
            trackable_id: $scope.task.id,
            trackable_type: 'Task',
            user_id: gon.current_user.uuid
          }).$promise.then(function(success) {
            $scope.notifications.unshift(success);
          });
        }
      );
    };


    $scope.log = function() {
      try{
      if ($scope.workedTime != "0h" || $scope.workedTime != "0m" ) {
        r = Duration.parse($scope.task.remaining);
        w = Duration.parse($scope.workedTime);
        t = Duration.parse($scope.task.worked);
        $scope.updateTask.remaining = new Duration(r - w).toString();
        $scope.updateTask.worked = new Duration(t + w).toString();
      } else {
        $scope.updateTask.worked = $scope.task.worked;
      }

      Task.update({
        projectID: $stateParams.project_id,
        taskID: $scope.task.id
      }, $scope.updateTask).$promise.then(
        function(success) {
          $scope.task = success;
          toastr.success('Task work logged!', 'Success!');
          $scope.workedTime = null;
          Notification.save({
            key: 'logged work in task',
            trackable_id: $scope.task.id,
            trackable_type: 'Task',
            user_id: gon.current_user.uuid
          }).$promise.then(function(success) {
            $scope.notifications.unshift(success);
          });
        });
      } catch(error){
        toastr.error('Occuerd error with log work!', 'Faliure!');
      }

    };
    $scope.estimate = function() {
      //$scope.updateTask = $scope.task;
      Task.update({
        projectID: $stateParams.project_id,
        taskID: $scope.task.id
      }, {estimation: $scope.updateTask.estimation, remaining: $scope.updateTask.estimation, worked: "0h"}).$promise.then(
        function(success) {
          $scope.task = success;
          toastr.success('Task estimated!', 'Success!');
        }, function(error){
            toastr.error('Occuerd error with estimation!', 'Faliure!');
        });
      Notification.save({
        key: 'estimate ' + $scope.updateTask.estimation + ' in task',
        trackable_id: $scope.task.id,
        trackable_type: 'Task',
        user_id: gon.current_user.uuid
      }).$promise.then(function(success) {
        $scope.notifications.unshift(success);
      });
    };
    $scope.setStatus = function(val){
      $scope.updateTask.status = val;
    };
  }
]);
