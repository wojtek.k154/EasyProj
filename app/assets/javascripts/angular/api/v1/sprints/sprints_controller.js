app1.controller("SprintsCtrl", ['$scope', 'Project', 'User', 'Sprint', 'Task', 'Notification', '$stateParams', 'Auth',
  function($scope, Project, User, Sprint, Task, Notification, $stateParams, Auth) {
    Auth.currentUser().then(function(user) {
      gon.current_user = user;
      }, function(error) {
        $state.go('startpage');
    });
    $scope.loading = true;
    $scope.sprints = {
      number: null,
      spr: [],
      created: {
        status: "Created",
        items: []
      },
      current: {
        status: "InProgres",
        items: []
      },
      closed: {
        status: "Closed",
        items: []
      }
    };
    Project.get({
      projectID: $stateParams.project_id
    }).$promise.then(
      function(success) {
        $scope.project = success;
        $scope.tasks = $scope.project.tasks;
        $scope.sprints.number = $scope.project.sprints.length;
        for (var i = 0; i < $scope.project.sprints.length; i++) {
          if ($scope.project.sprints[i].status == 'Created') {
            $scope.sprints.created.items.push($scope.project.sprints[i]);
          } else if ($scope.project.sprints[i].status == 'InProgres') {
            $scope.sprints.current.items.push($scope.project.sprints[i]);
          } else if ($scope.project.sprints[i].status == 'Closed') {
            $scope.sprints.closed.items.push($scope.project.sprints[i]);
          }
        }
      },
      function(error) {

      }
    ).finally(function() {
      $scope.loading = false;
    });

    $scope.tempsprint = {};
    $scope.newSprint = {
      status: "Created"
    };
    $scope.updatedSprint = {};

    $scope.setUpdateSprint = function(p, i) {
      $scope.tempsprint = {
        status: p.status,
        index: i
      };
      $scope.updatedSprint = p;
      if (p.start_date) {
        $scope.updatedSprint.start_date = new Date(p.start_date);
      }
      if (p.end_date) {
        $scope.updatedSprint.end_date = new Date(p.end_date);
      }
    };
    $scope.addSprint = function() {
      Sprint.save({
        projectID: $stateParams.project_id
      }, $scope.newSprint).$promise.then(
        function(success) {
          $scope.sprints.created.items.unshift(success);
          $scope.sprints.number += 1;
          $scope.sprintCreate = true;
          toastr.success('Success!', 'Sprint is successfuly created!');
          $scope.newSprint = {
            status: "Created"
          };
          Notification.save({
            key: 'added new sprint',
            trackable_id: success.id,
            trackable_type: 'Sprint',
            user_id: gon.current_user.uuid
          });
        },
        function(error) {
          toastr.error('Faliure!', 'Errors while adding new sprint!');
          $scope.errors = error;
        }
      )
    };
    $scope.closeSprint = function(sprint) {
      Task.query({
        projectID: $stateParams.project_id,
        sprint_id: sprint.id
      }).$promise.then(
        function(success) {

          for (var i = 0; i < success.length; i++) {
            if (success[i].status != 'Closed') {
              Task.update({
                projectID: $stateParams.project_id,
                taskID: success[i].id
              }, {
                sprint_id: null,
                status: 'Created'
              });
            }
          }
        }
      )
    };
    $scope.updateSprint = function() {
      Sprint.update({
        projectID: $scope.updatedSprint.project_id,
        sprintID: $scope.updatedSprint.id
      }, $scope.updatedSprint).$promise.then(
        function(success) {
          if ($scope.tempsprint.status == 'Created' && success.status != $scope.tempsprint.status) {
            $scope.sprints.created.items.splice($scope.tempsprint.index, 1);
            toastr.success('Success!', 'Sprint set status as InProgres');
          }
          if ($scope.tempsprint.status == 'InProgres' && success.status != $scope.tempsprint.status) {
            $scope.sprints.current.items.splice($scope.tempsprint.index, 1);
            toastr.success('Success!', 'Sprint set status as Closed');
          }
          if (success.status == 'InProgres' && success.status != $scope.tempsprint.status) {
            $scope.sprints.current.items.push(success);
            Notification.save({
              key: 'set status as InProgres in sprint',
              trackable_id: success.id,
              trackable_type: 'Sprint',
              user_id: gon.current_user.uuid
            });
          }
          if (success.status == 'Closed' && success.status != $scope.tempsprint.status) {
            $scope.sprints.closed.items.push(success);
            Notification.save({
              key: 'set status Closed in sprint',
              trackable_id: success.id,
              trackable_type: 'Sprint',
              user_id: gon.current_user.uuid
            });
          }
          $scope.updatedSprint = {};
          $scope.sprintUpdate = true;
          if (success.status == 'Closed') {
            $scope.closeSprint(success);
          }
        },
        function(error) {
          toastr.error('Faliure!', 'Ocurred errors while changing status of sprint!');
          $scope.errors = error;
        }
      )
    };

    $scope.deleteSprint = function(Id, index) {
      Sprint.delete({
        projectID: $stateParams.project_id,
        sprintID: Id
      }).$promise.then(
        function(success) {
          toastr.success('Success!', 'Sprint was successfuly deleted!');
          $scope.sprints.created.items.splice(index, 1);
          Notification.save({
            key: 'deleted sprint sprint',
            user_id: gon.current_user.uuid
          });
        },
        function(error) {
          toastr.error('Faliure!', 'Ocurred errors while deleting sprint!');
          Sprint.query({
            projectID: $stateParams.project_id
          }).$promise.then(
            function(success) {
              $scope.sprints = success;
            }
          );
        }
      )
    };
  }
]);


app1.controller("SprintCtrl", ['$scope', 'Project', 'User', 'Sprint', 'Task', 'Notification', '$stateParams', 'Auth',
  function($scope, Project, User, Sprint, Task, Notification, $stateParams, Auth) {
    $scope.loading = true;
    $scope.est = {
      backlog: 0,
      sprint: 0
    };
    $scope.newTask = {
      sprint_id: $stateParams.sprint_id,
      status: "Created",
      user_id: null
    };
    Auth.currentUser().then(function(user) {
      gon.current_user = user;
      }, function(error) {
        $state.go('startpage')
    });
    $scope.tempUser = {};
    $scope.newTask = {
      status: "Created"
    };
    $scope.setTaskNew = function() {
      $scope.newTask = {
        status: "Created",
        worked: "0h"
      };
    };

    $scope.lists = {
      unsigned: {
        name: "Backlog",
        items: []
      },
      signed: {
        name: "Sprint's tasks",
        items: []
      }
    };
    Project.get({
      projectID: $stateParams.project_id
    }).$promise.then(
      function(success) {
        $scope.project = success;
        $scope.users = $scope.project.users;
        for (var i = 0; i < $scope.project.sprints.length; i++) {
          if ($scope.project.sprints[i].id == $stateParams.sprint_id) {
            $scope.sprint = $scope.project.sprints[i];
          }
        }
        for (var i = 0; i < $scope.users.length; i++) {
          if ($scope.users[i].id == gon.current_user.id) {
            $scope.users.splice(i, 1);
          }
        }
        for (var i = 0; i < $scope.project.tasks.length; i++) {
          if ($scope.project.tasks[i].sprint_id == $stateParams.sprint_id) {
            $scope.lists.signed.items.push($scope.project.tasks[i]);
          } else if ($scope.project.tasks[i].sprint_id == null) {
            $scope.lists.unsigned.items.push($scope.project.tasks[i]);
          }
        }
        $scope.countTaskTime();
      }
    ).finally(function() {
      $scope.loading = false;
    });

    $scope.addTask = function() {
      $scope.newTask.sprint_id = $stateParams.sprint_id;
      $scope.newTask.remaining = $scope.newTask.estimation;
      $scope.newTask.worked = "0h";
      Task.save({
        projectID: $stateParams.project_id
      }, $scope.newTask).$promise.then(
        function(success) {
          $scope.tempUser = {};
          $scope.lists.signed.items.unshift(success);
          $scope.createTask = true;
          $scope.newTask = {
            sprint_id: $stateParams.sprint_id,
            status: "Created"
          };
          toastr.success('Success!', 'New task added!');
          Notification.save({
            key: 'added to project' + $scope.project.name + ' a new task',
            trackable_id: success.id,
            trackable_type: 'Task',
            user_id: gon.current_user.uuid
          });
          $scope.tempUser = {
            email: null,
            name: null
          };
          $scope.countTaskTime();
        },
        function(error) {
          $scope.errors = error;
          toastr.error('Faliure!', 'Occured error!');
        }
      )
    };

    $scope.assignUserToTask = function(p) {
      if (typeof p == "undefined") {
        $scope.newTask.user_id = gon.current_user.uuid;
        $scope.tempUser = gon.current_user;
      } else {
        $scope.newTask.user_id = p.uuid;
        $scope.tempUser = p;
      }
    };

    $scope.removeTaskFromSprint = function(task, index) {
      $scope.lists.signed.items.splice(index, 1);
      Task.update({
        projectID: $stateParams.project_id,
        taskID: task.id
      }, {
        sprint_id: null
      }).$promise.then(function(success) {
        toastr.success('Success!', 'Task removed!');
        Notification.save({
          key: 'removed from ' + $scope.sprint.name + 'sprint a exisiting task',
          trackable_id: success.id,
          trackable_type: 'Task',
          user_id: gon.current_user.uuid
        });
      }, function(error) {
        $scope.errors = error;
        toastr.error('Faliure!', 'Occured error!');
      });
      $scope.countTaskTime();
    };
    $scope.addTaskToSprint = function(task, index) {
      $scope.lists.unsigned.items.splice(index, 1);
      Task.update({
        projectID: $stateParams.project_id,
        taskID: task.id
      }, {
        sprint_id: $stateParams.sprint_id
      }).$promise.then(function(success) {
        toastr.success('Success!', 'Task added form sprint!');
        Notification.save({
          key: 'added to sprint ' + $scope.sprint.name + ' an exisiting task',
          trackable_id: success.id,
          trackable_type: 'Task',
          user_id: gon.current_user.uuid
        });
      });
      $scope.countTaskTime();
    };
    $scope.countTaskTime = function() {
      var est = {
        backlog: 0,
        sprint: 0
      };
      for (var i = 0; i < $scope.lists.signed.items.length; i++) {
        if ($scope.lists.signed.items[i].estimation) {
          est.sprint += Duration.parse($scope.lists.signed.items[i].estimation);
        }
      }

      for (var i = 0; i < $scope.lists.unsigned.items.length; i++) {
        if ($scope.lists.unsigned.items[i].estimation) {
          est.backlog += Duration.parse($scope.lists.unsigned.items[i].estimation);
        }
      }

      $scope.est.sprint = new Duration(est.sprint).toString();
      $scope.est.backlog = new Duration(est.backlog).toString();
    };

    $scope.setUpdateTask = function(wp, i) {
      $scope.i = i;
      $scope.newTask = wp;
      if (wp.begin) {
        $scope.newTask.begin = new Date(wp.begin);
      }
      if (wp.finish) {
        $scope.newTask.finish = new Date(wp.finish);
      }
    };

    $scope.setKind = function(val) {
      $scope.newTask.kind = val;
    };
    $scope.setPriority = function(val) {
      $scope.newTask.priority = val;
    };
  }
]);
