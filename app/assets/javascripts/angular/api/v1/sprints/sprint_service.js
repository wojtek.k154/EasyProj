app1.factory('Sprint', ['$resource', function ($resource) {
    return $resource("/api/v1/projects/:projectID/sprints/:sprintID", {projectID: "@project_id", sprintID: "@id"},{
          query: { method: 'GET', isArray: true },
          update: { method:'PUT' },
          get: { method: 'GET', isArray: false }
    });
}]);
