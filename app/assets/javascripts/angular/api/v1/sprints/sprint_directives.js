app1.directive('sprintlisttodo', function(){
  return {
    restrict: 'E',
    templateUrl: 'api/v1/sprints/directives/sprint-list-todo.html'
  };
});
app1.directive('sprintlistcurrent', function(){
  return {
    restrict: 'E',
    templateUrl: 'api/v1/sprints/directives/sprint-list-current.html'
  };
});
app1.directive('sprintlistclosed', function(){
  return {
    restrict: 'E',
    templateUrl: 'api/v1/sprints/directives/sprint-list-closed.html'
  };
});
app1.directive('sprintform', function(){
  return {
    restrict: 'E',
    templateUrl: 'api/v1/sprints/directives/form.html'
  };
});
app1.directive('projectheader', function() {
  return {
    restrict: 'E',
    controllerAs: 'SprintsCtrl',
    templateUrl: 'api/v1/sprints/directives/projectheader.html'
  };
});
