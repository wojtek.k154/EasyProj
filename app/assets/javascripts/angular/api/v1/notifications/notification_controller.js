app1.controller("NotificationCtrl", ['$scope','$state', 'Auth', 'Notification', function($scope, $state, Auth, Notification){
  $scope.loading =  true;
  $scope.user_projects = [];

  Auth.currentUser().then(function(user) {
    gon.current_user = user;
    }, function(error) {
      $state.go('startpage')
  });
  $scope.notifications = [];

  Notification.query().$promise.then(
    function (success) {
      for(var i = 0; i < success.length; i++){
        for(var j = 0; j < $scope.user_projects.length; j++){
          if (success[i].trackable_type == 'Project'){
            if ($scope.user_projects[j].id == success[i].trackable_id) {
              $scope.notifications.push(success[i]);
            }
          } else {
            if ($scope.user_projects[j].id == success[i].trackable.project_id) {
              $scope.notifications.push(success[i]);
            }
          }
        }
      }
    }
  ).finally(function() {
    $scope.loading = false;
  });

}]);
