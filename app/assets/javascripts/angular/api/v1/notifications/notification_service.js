app1.factory("Notification", ['$resource', function ($resource) {
    return $resource("/api/v1/notifications", {},{
      query: { method: 'GET', isArray: true }
    });
}]);
