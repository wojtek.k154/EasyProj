app1.factory("Project", ['$resource', function ($resource) {
    return $resource("/api/v1/projects/:projectID", {projectID: "@id"},{
          update: { method:'PUT' },
          get: { method: 'GET', isArray: false }
    });
}]);
