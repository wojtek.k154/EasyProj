app1.controller("ChartCtrl", ['$scope', 'Project', 'Auth', 'User', 'Sprint', 'Task', '$stateParams',
  function($scope, Project, Auth, User, Sprint, Task, $stateParams) {
    Auth.currentUser().then(function(user) {
      gon.current_user = user;
      }, function(error) {
        $state.go('startpage')
    });
    $scope.loading = true;
    $scope.chartsStatus = {
      todo:{
        items: [],
        nr: 0
      },
      inprogres:{
        items: [],
        nr: 0
      },
      closed: {
        items: [],
        nr: 0
      }
    };
    var chartdata = [];

    Project.get({ projectID: $stateParams.project_id}).$promise.then(
      function(success){
        $scope.project = success;
      }
    );

    Task.query({ projectID: $stateParams.project_id }).$promise.then(
      function(success) {
        $scope.tasks = success;
        $scope.sortbyStatus();
      }
    ).finally(function() {
      $scope.loading = false;

    });
    $scope.sortbyStatus = function(){
      for(var i = 0; i < $scope.tasks.length; i++){
        if($scope.tasks[i].status == 'Created'){
          $scope.chartsStatus.todo.items.push($scope.tasks[i]);
          $scope.chartsStatus.todo.nr+=1;
        } else if ($scope.tasks[i].status == 'InProgres') {
          $scope.chartsStatus.inprogres.items.push($scope.tasks[i]);
          $scope.chartsStatus.inprogres.nr+=1;
        } else {
          $scope.chartsStatus.closed.items.push($scope.tasks[i]);
          $scope.chartsStatus.closed.nr+=1;
        }
      }
      chartdata.push($scope.chartsStatus.todo.nr);
      chartdata.push($scope.chartsStatus.inprogres.nr);
      chartdata.push($scope.chartsStatus.closed.nr);
    };

    $scope.chartConfig = {
      options: {
        chart: {
          type: 'column'
        },
        plotOptions: {
          series: {
            stacking: ''
          }
        }
      },
      series: [{
        names:['Created', 'InProgres', 'Closed'],
        data: chartdata//[$scope.chartsStatus.todo.nr, $scope.chartsStatus.inprogres.nr, $scope.chartsStatus.closed.nr]
      }],
      title: {
        text: 'Tasks status'
      },
      subtitle: {
        text: 'Chart describeing numbers of tasks with assigned status'
      },
      xAxis: {
        categories: ['Created', 'InProgres', 'Closed'],
        title: {
          text: 'Status'
        }
      },
      yAxis: {

        title: {
          text: 'Task number'
        }
      },
      credits: {
        enabled: true
      },
      loading: false,
      size: {}
    };
  }
]);
