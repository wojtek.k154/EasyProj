app1.directive('projectlist', function() {
  return {
    restrict: 'E',
    templateUrl: 'api/v1/projects/directives/project-list.html'
  };
});
app1.directive('projectheaderchart', function() {
  return {
    restrict: 'E',
    controllerAs: 'SprintsCtrl',
    templateUrl: 'api/v1/sprints/directives/projectheader.html'
  };
});
