app1.controller("ProjectsCtrl", ['$scope', '$state', 'Project', 'Auth', 'Notification', 'User',
  function($scope, $state, Project, Auth, Notification, User) {
    $scope.loading = true;
    $scope.newProject = {
      status: "Created",
    };
    Auth.currentUser().then(function(user) {
      gon.current_user = user;
      Project.query().$promise.then(
        function(success) {
          $scope.user_projects = success;
        }
      );
    }, function(error) {
      $state.go('startpage');
    }).finally(function() {
      $scope.loading = false;
    });
    $scope.updateProject = {};

    $scope.isOwner = function(uid) {
      if (uid == gon.current_user.uuid) {
        return true;
      } else {
        return false;
      }
    };

    $scope.setUpdateProject = function(p) {
      $scope.updateProject = p;
    };
    $scope.addProject = function() {
      $scope.newProject.owner_id = gon.current_user.uuid;
      Project.save($scope.newProject).$promise.then(
        function(success) {
          $scope.user_projects.unshift(success);
          $scope.projectCreated = true;
          $scope.errors = null;
          $scope.newProject = {
            status: "Created",
            owner_id: gon.current_user.id
          };
          toastr.success('You added new project!', 'Success!');
          Notification.save({
            key: 'created a new project',
            trackable_id: success.id,
            trackable_type: 'Project',
            user_id: gon.current_user.uuid
          });
          $('.fade').remove();
          $state.go('nav.project.sprints', {
            reload: true,
            project_id: success.id
          });
        },
        function(error) {
          $scope.errors = error;
          toastr.error('Occured error!', 'Faliure!');
        }
      )
    };
    $scope.updateProject = function() {
      Project.update({
        id: $scope.updateProject.id
      }, $scope.updateProject).$promise.then(
        function(success) {
          $scope.user_projects = Project.query();
          toastr.success('Project is successfuly updated!', 'Success!');
          Notification.save({
            key: 'updated a project',
            trackable_id: success.id,
            trackable_type: 'Project',
            user_id: gon.current_user.uuid
          });
        },
        function(error) {
          $scope.errors = error;
          toastr.error('Occured error!', 'Faliure!');
        }
      )
    };
    $scope.deleteProject = function(Id, index) {
      Project.delete({
        projectID: Id
      }).$promise.then(
        function(success) {
          toastr.success('Project is successfuly deleted!', 'Success!');
          $scope.user_projects.splice(index, 1);
        },
        function(error) {
          $scope.errors = error;
          toastr.error('Occured error!', 'Faliure!');
        }
      )
    };

  }
]);


app1.controller("ProjectCtrl", ['$scope', '$state', 'Auth', 'Project', 'User', 'Sprint', 'Task', '$stateParams', function($scope, $state, Auth, Project, User, Sprint, Task, $stateParams) {
  $scope.loading = true;
  $scope.notifications = [];
  Auth.currentUser().then(function(user) {
    gon.current_user = user;
  }, function(error) {
    $state.go('startpage')
  });
  Project.get({
    projectID: $stateParams.project_id
  }).$promise.then(
    function(success) {
      var tempArray = [];
      $scope.project = success;
      $scope.notifications.push(success.notifications);
      $scope.notifications = [].concat.apply([], tempArray);
    }
  );

  Task.query({
    projectID: $stateParams.project_id
  }).$promise.then(
    function(success) {
      var tempArray = [];
      $scope.tasks = success;
      for (var i = 0; i < success.length; i++) {
        tempArray.push(success[i].notifications);
      }
      $scope.notifications = [].concat.apply([], tempArray);
    }
  ).finally(function() {
    $scope.loading = false;
  });
}]);
