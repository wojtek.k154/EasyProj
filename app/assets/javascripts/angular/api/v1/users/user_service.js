app1.factory('User',['$resource', function ($resource) {
    return $resource("/api/v1/users/:uuid", {uuid: "@uuid"}, {
       'update': { method:'PUT' },
       'get':    {method:'GET'}
    });
}]);
