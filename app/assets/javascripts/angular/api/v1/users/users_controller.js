app1.controller("UserCtrl", ['$scope', 'User', 'Task', 'Project', '$state', 'Auth', 'Notification', function($scope, User, Task, Project, $state, Auth, Notification) {
  Auth.currentUser().then(function(user) {
    $scope.loading = true;
    $scope.user = user;
    $scope.projects = $scope.user.projects;
    $scope.tasks = $scope.user.tasks;
    $scope.setChart($scope.tasks);
  }, function(error) {
    $state.go('startpage');
  }).finally(function() {
    $scope.loading = false;
  });
  $scope.tasksChart = {
    estimate: [],
    worked: [],
    task: []
  };

  $scope.setChart = function(tasks) {
    for (var i = 0; i < tasks.length; i++) {
      if (tasks[i].estimation == null) {
        $scope.tasksChart.estimate.push(0);
      } else {
        $scope.tasksChart.estimate.push(Duration.parse($scope.tasks[i].estimation).hours());
      }
      if (tasks[i].worked == null) {
        $scope.tasksChart.worked.push(0);
      } else {
        $scope.tasksChart.worked.push(Duration.parse($scope.tasks[i].worked).hours());
      }

      $scope.tasksChart.task.push(tasks[i].name);
    }
  };
  $scope.chartConfig = {
    options: {
      chart: {
        type: 'column'
      },
      plotOptions: {
        series: {
          stacking: ''
        }
      }
    },
    series: [{
      name: "Estimation time",
      data: $scope.tasksChart.estimate
    }, {
        name: "Work time",
        data: $scope.tasksChart.worked
    }],
    title: {
      text: 'Tasks status'
    },
    subtitle: {
      text: 'Chart describeing numbers of tasks with assigned status'
    },
    xAxis: {
      categories: $scope.tasksChart.task,
      title: {
        text: 'Status'
      }
    },
    yAxis: {

      title: {
        text: 'Hours'
      }
    },
    credits: {
      enabled: true
    },
    loading: false,
    size: {}
  };

}]);
app1.controller("UsersCtrl", ['$scope', 'User', 'Project', 'Sprint', 'Notification', '$stateParams', 'Auth', function($scope, User, Project, Sprint, Notification, $stateParams, Auth) {
  $scope.loading = true;
  Auth.currentUser().then(function(user) {
    gon.current_user = user;
    }, function(error) {
      $state.go('startpage')
  });
  $scope.users = {
    unsigned: {
      name: "Unsigned",
      items: []
    },
    signed: {
      signed: "Signed",
      items: []
    }
  };

  Project.get({
    projectID: $stateParams.project_id
  }).$promise.then(
    function(success) {
      $scope.project = success;
      $scope.sprints = {
        number: $scope.project.sprints.length
      };
    }
  );
  User.query().$promise.then(
    function(success) {
      $scope.success = success;
      for (var i = 0; i < success.length; i++) {
        $scope.flag = false;
        if (success[i].projects.length == 0) {
          $scope.users.unsigned.items.push(success[i]);
        } else {

          for (var j = 0; j < success[i].projects.length; j++) {
            if (success[i].projects[j].id == $stateParams.project_id) {
              $scope.users.signed.items.push(success[i]);
              $scope.flag = false;
              break;
            } else {
              $scope.flag = true;
            }
          }
          if ($scope.flag) {
            $scope.users.unsigned.items.push(success[i]);
            $scope.flag = false;
          }
        }
      }
    }
  ).finally(function() {
    $scope.loading = false;
  });

  $scope.addToProject = function(item, index) {
    Project.update({
      projectID: $stateParams.project_id
    }, {
      newuser: item.uuid
    }).$promise.then(
      function(success) {
        Notification.save({
          key: 'was added to project',
          trackable_id: $stateParams.project_id,
          trackable_type: 'Project',
          user_id: item.uuid
        });
        $scope.users.unsigned.items.splice(index, 1);

      },
      function(error) {
        $scope.errors = error;
        toastr.error('Faliure!', 'Occured error!');
      }
    );
  };
  $scope.removeFromProject = function(item, index) {
    Project.update({
      projectID: $stateParams.project_id
    }, {
      remove_user_project: item.uuid
    }).$promise.then(
      function(success) {
        Notification.save({
          key: 'was removed from project',
          trackable_id: $stateParams.project_id,
          trackable_type: 'Project',
          user_id: item.uuid
        });
        $scope.users.signed.items.splice(index, 1);
      }
    );
  };
}]);

app1.controller("UserUpdateCtrl", ['$scope', '$stateParams', 'User', 'Upload', '$state', 'Auth', function($scope, $stateParams, User, Upload, $state, Auth) {
  Auth.currentUser().then(function(user) {
    $scope.loading = true;
    $scope.user = user;
    $scope.thisUser = $scope.user;
  }, function(error) {
    $state.go('startpage');
  }).finally(function() {
    $scope.loading = false;
  });
  $scope.updateCurrentUser = function(){
    Upload.upload({
        url: '/api/v1/users/'+$scope.user.uuid,
        method: 'PUT',
        data: {
          user: $scope.thisUser
        }
    }).then(function(success) {
      $scope.thisUser = success.data;
      $scope.$parent.user = success.data;
      toastr.success('User successfuly updated!', 'Success!');
    }, function(error) {
      toastr.error('Occured error!', 'Faliure!');
    });
  };
}]);
