app1.directive('projectheadertwo', function() {
  return {
    restrict: 'E',
    controllerAs: 'SprintsCtrl',
    templateUrl: 'api/v1/sprints/directives/projectheader.html'
  };
});
