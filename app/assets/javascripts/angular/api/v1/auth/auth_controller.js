app1.controller("AuthCtrl", ['$scope', '$state', 'Upload', 'Auth', '$stateParams', function($scope, $state, Upload, Auth, $stateParams) {
  $scope.params = {
    password: '',
    password_confirmation: '',
    reset_password_token: '',
  };

  $scope.login = function() {
    Auth.login($scope.user).then(function(resp) {
      $state.go('nav.projects');
      toastr.success('User successfuly logged up!', 'Success!');
    }, function(error) {
      toastr.error('Occured Login errors!', 'Faliure!');
    });
  };

  $scope.isLoggedIn = function(){
    if($state.current.name == 'login')
      Auth.currentUser().then(function(user) {
        $state.go('nav.projects');
      }, function(error) {
      console.log('niezalogowany');
    })
  };
  $scope.isLoggedIn();

  $scope.uploadUser = function() {
    Upload.upload({
        url: '/users.json',
        method: 'POST',
        data: {
          user: $scope.user
        }
    }).then(function(success) {
      $state.go('nav.projects');
      toastr.success('User successfuly signed up!', 'Success!');
    }, function(error) {
      toastr.error(error, 'Faliure!');
    });
  };
  $scope.signup = function() {
    $scope.uploadUser();
  };

  $scope.sendInstructions = function() {
    Auth.sendResetPasswordInstructions($scope.user).then(function(success) {
      toastr.success('Password reset instructions send!', 'Success!'); // Sended email if user found otherwise email not sended...
      $state.go('login');
    }, function(error) {
      toastr.error('Occured error', 'Faliure!');
    });
  };
  $scope.resetPassword = function() {
    $scope.params.reset_password_token = $stateParams.reset_token;
    Auth.resetPassword($scope.params).then(function(new_data) {
      toastr.success('Password successfuly reseted!', 'Success!');
      $state.go('login')
    }, function(error) {
      toastr.error('Occured reset assword errors!', 'Faliure!');
    });
  };
}]);
