class ProjectDecorator < Draper::Decorator
  delegate_all

  def owner_email
    object.owner.email
  end
end
