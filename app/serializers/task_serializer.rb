class TaskSerializer < ActiveModel::Serializer
  attributes :id, :sprint_id, :project_id, :user_id, :name, :description, :status,
             :estimation, :remaining, :worked, :begin, :finish, :user, :created_at, :updated_at,
             :project_name, :kind, :priority, :created_at, :updated_at

  def user
    object.user
  end

  def project_name
    decorated.task_project
  end

  def decorated
    @decorated ||= TaskDecorator.new(object)
  end
end
