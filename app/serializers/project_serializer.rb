class ProjectSerializer < ActiveModel::Serializer
  attributes :id, :name, :description, :status, :owner_id, :newuser, :remove_user_project,
    :users, :sprints, :tasks

  has_many :sprints
  has_many :tasks

  def owner_name
    decorated.owner_email
  end
  def decorated
    @decorated ||= ProjectDecorator.new(object)
  end
end
