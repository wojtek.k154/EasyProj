class NotificationSerializer < ActiveModel::Serializer
  attributes :id, :trackable_type, :trackable_id, :key, :user, :trackable, :created_at
  def user
    object.user
  end

  def trackable
    object.trackable
  end
end
