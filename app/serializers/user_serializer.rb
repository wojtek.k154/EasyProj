class UserSerializer < ActiveModel::Serializer
  attributes :email, :name, :avatar, :projects, :tasks, :uuid

  def projects
    object.projects
  end

  def tasks
    object.tasks
  end
end
