class SprintSerializer < ActiveModel::Serializer
  attributes :id, :project_id, :name, :start_date, :end_date, :status

  belongs_to :project
end
