FactoryGirl.define do
  factory :user do
    email 'test@example.com'
    name 'John Smith'
    password 'test1234'
  end

  factory :project do
    name 'Test11 project11'
    description 'Test project description1112211'
    status 'Created'
    owner_id '107f3601-af4c-4024-a869-5635a3333d03'
  end

  factory :sprint do
    name 'Test Sprint'
    start_date Date.today
    end_date Date.today + 1.week
    status 'Created'
  end

  factory :task do
    kind 'Task'
    priority 1
    name 'Test Task'
    description 'Test task description'
    status 'Created'
    estimation '4h 45m'

  end

  factory :notification do
    key "added new task"
  end
end
