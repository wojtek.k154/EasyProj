describe('Controller: ChartCtrl', function() {
  var scope, createController,mockProject, mockTask;
  beforeEach(module('Easypro'));

  beforeEach(inject(function($rootScope, $controller, Project, Task) {
    scope=$rootScope.$new();
    mockProject = Project;
    mockTask = Task;
    spyOn(mockProject, 'get').and.callThrough();
    spyOn(mockTask, 'query').and.callThrough();
    createController = $controller('ChartCtrl', {
        $scope : scope,
        Project : mockProject,
        Task : mockTask
    });

  }));
  it('should be defined and call services', function() {
    expect($controller).toBeDefined();
    expect(mockProject.get).toHaveBeenCalled();
    expect(mockTask.query).toHaveBeenCalled();
  });
  it('get project', function() {

  });
  it('task quey', function() {});
  it('sort task by status', function() {});

});
/*
{
  "id": 7,
  "name": "Softprojecq",
  "description": "Krest ",
  "status": "Created",
  "owner_name": "1test2@example.com",
  "owner_id": "f22e1746-1a1f-41cb-a6dc-4e70d8c2860a",
  "newuser": null,
  "remove_user_project": null,
  "users": [{
    "id": "7af1b9d0-06fb-4fe8-8a80-8d167e92bbcf",
    "email": "1test@example.com",
    "name": "Test Test1",
    "created_at": "2016-09-11T11:00:24.120Z",
    "updated_at": "2016-09-12T20:24:04.440Z",
    "avatar_file_name": "ca.jpg",
    "avatar_content_type": "image/jpeg",
    "avatar_file_size": 13292,
    "avatar_updated_at": "2016-09-11T11:00:24.081Z",
    "uuid": "7af1b9d0-06fb-4fe8-8a80-8d167e92bbcf"
  }, {
    "id": "d8313b09-945f-4bf5-a735-f926a11319d8",
    "email": "1test125@example.com",
    "name": "Test5 Testd51",
    "created_at": "2016-09-11T11:00:28.375Z",
    "updated_at": "2016-09-23T13:31:14.105Z",
    "avatar_file_name": "a0.jpg",
    "avatar_content_type": "image/jpeg",
    "avatar_file_size": 10282,
    "avatar_updated_at": "2016-09-11T11:00:28.363Z",
    "uuid": "d8313b09-945f-4bf5-a735-f926a11319d8"
  }],
  "sprints": [{
    "id": 3,
    "project_id": 7,
    "name": "First Sprint",
    "start_date": "2016-09-11",
    "end_date": "2016-09-18",
    "status": "InProgres"
  }, {
    "id": 6,
    "project_id": 7,
    "name": "ZXZxZxZxasSACASCSAA",
    "start_date": "2016-09-12",
    "end_date": "2016-09-14",
    "status": "Closed"
  }],
  "tasks": [{
    "id": 11,
    "sprint_id": 5,
    "project_id": 7,
    "user_id": "f22e1746-1a1f-41cb-a6dc-4e70d8c2860a",
    "name": "Third Task",
    "description": "description Third Second Fifith Task",
    "status": "Created",
    "estimation": "2h",
    "remaining": null,
    "worked": null,
    "begin": null,
    "finish": null,
    "user": {
      "id": "f22e1746-1a1f-41cb-a6dc-4e70d8c2860a",
      "email": "1test2@example.com",
      "name": "Test2 Test21",
      "created_at": "2016-09-11T11:00:27.002Z",
      "updated_at": "2016-09-24T22:01:24.763Z",
      "avatar_file_name": "aa.jpg",
      "avatar_content_type": "image/jpeg",
      "avatar_file_size": 29386,
      "avatar_updated_at": "2016-09-11T11:00:26.994Z",
      "uuid": "f22e1746-1a1f-41cb-a6dc-4e70d8c2860a"
    },
    "created_at": "2016-09-11T11:00:29.837Z",
    "updated_at": "2016-09-24T19:13:11.065Z",
    "project_name": "Softprojecq",
    "kind": "SubTask",
    "priority": 2
  }, {
    "id": 19,
    "sprint_id": 5,
    "project_id": 7,
    "user_id": "f22e1746-1a1f-41cb-a6dc-4e70d8c2860a",
    "name": "Fifith Task",
    "description": "description Fifith Fifith Task",
    "status": "Created",
    "estimation": null,
    "remaining": null,
    "worked": null,
    "begin": null,
    "finish": null,
    "user": {
      "id": "f22e1746-1a1f-41cb-a6dc-4e70d8c2860a",
      "email": "1test2@example.com",
      "name": "Test2 Test21",
      "created_at": "2016-09-11T11:00:27.002Z",
      "updated_at": "2016-09-24T22:01:24.763Z",
      "avatar_file_name": "aa.jpg",
      "avatar_content_type": "image/jpeg",
      "avatar_file_size": 29386,
      "avatar_updated_at": "2016-09-11T11:00:26.994Z",
      "uuid": "f22e1746-1a1f-41cb-a6dc-4e70d8c2860a"
    },
    "created_at": "2016-09-11T11:00:29.926Z",
    "updated_at": "2016-09-24T17:27:00.649Z",
    "project_name": "Softprojecq",
    "kind": "SubTask",
    "priority": 3
  }, {
    "id": 3,
    "sprint_id": 5,
    "project_id": 7,
    "user_id": "f22e1746-1a1f-41cb-a6dc-4e70d8c2860a",
    "name": "First Task",
    "description": "description First Task Fifith",
    "status": "Created",
    "estimation": "2h 36m",
    "remaining": "2h 36m",
    "worked": "0h",
    "begin": null,
    "finish": null,
    "user": {
      "id": "f22e1746-1a1f-41cb-a6dc-4e70d8c2860a",
      "email": "1test2@example.com",
      "name": "Test2 Test21",
      "created_at": "2016-09-11T11:00:27.002Z",
      "updated_at": "2016-09-24T22:01:24.763Z",
      "avatar_file_name": "aa.jpg",
      "avatar_content_type": "image/jpeg",
      "avatar_file_size": 29386,
      "avatar_updated_at": "2016-09-11T11:00:26.994Z",
      "uuid": "f22e1746-1a1f-41cb-a6dc-4e70d8c2860a"
    },
    "created_at": "2016-09-11T11:00:29.750Z",
    "updated_at": "2016-09-24T17:25:22.488Z",
    "project_name": "Softprojecq",
    "kind": "Task",
    "priority": 1
  }, {
    "id": 22,
    "sprint_id": null,
    "project_id": 7,
    "user_id": "7af1b9d0-06fb-4fe8-8a80-8d167e92bbcf",
    "name": "sadasdas",
    "description": "asdasdasdasdasdsa",
    "status": "Created",
    "estimation": "2h 23m",
    "remaining": "2h 23m",
    "worked": null,
    "begin": null,
    "finish": null,
    "user": {
      "id": "7af1b9d0-06fb-4fe8-8a80-8d167e92bbcf",
      "email": "1test@example.com",
      "name": "Test Test1",
      "created_at": "2016-09-11T11:00:24.120Z",
      "updated_at": "2016-09-12T20:24:04.440Z",
      "avatar_file_name": "ca.jpg",
      "avatar_content_type": "image/jpeg",
      "avatar_file_size": 13292,
      "avatar_updated_at": "2016-09-11T11:00:24.081Z",
      "uuid": "7af1b9d0-06fb-4fe8-8a80-8d167e92bbcf"
    },
    "created_at": "2016-09-24T18:32:59.742Z",
    "updated_at": "2016-09-24T18:32:59.742Z",
    "project_name": "Softprojecq",
    "kind": "Task",
    "priority": 2
  }, {
    "id": 7,
    "sprint_id": 5,
    "project_id": 7,
    "user_id": "f22e1746-1a1f-41cb-a6dc-4e70d8c2860a",
    "name": "Second Task",
    "description": "description Second TaskF ifith",
    "status": "Created",
    "estimation": "2h",
    "remaining": null,
    "worked": null,
    "begin": null,
    "finish": null,
    "user": {
      "id": "f22e1746-1a1f-41cb-a6dc-4e70d8c2860a",
      "email": "1test2@example.com",
      "name": "Test2 Test21",
      "created_at": "2016-09-11T11:00:27.002Z",
      "updated_at": "2016-09-24T22:01:24.763Z",
      "avatar_file_name": "aa.jpg",
      "avatar_content_type": "image/jpeg",
      "avatar_file_size": 29386,
      "avatar_updated_at": "2016-09-11T11:00:26.994Z",
      "uuid": "f22e1746-1a1f-41cb-a6dc-4e70d8c2860a"
    },
    "created_at": "2016-09-11T11:00:29.794Z",
    "updated_at": "2016-09-24T19:12:39.636Z",
    "project_name": "Softprojecq",
    "kind": "Task",
    "priority": 1
  }, {
    "id": 15,
    "sprint_id": 3,
    "project_id": 7,
    "user_id": "f22e1746-1a1f-41cb-a6dc-4e70d8c2860a",
    "name": " Fourtj Task",
    "description": "description Fourtj Task Fifith",
    "status": "Created",
    "estimation": "7h",
    "remaining": null,
    "worked": null,
    "begin": null,
    "finish": null,
    "user": {
      "id": "f22e1746-1a1f-41cb-a6dc-4e70d8c2860a",
      "email": "1test2@example.com",
      "name": "Test2 Test21",
      "created_at": "2016-09-11T11:00:27.002Z",
      "updated_at": "2016-09-24T22:01:24.763Z",
      "avatar_file_name": "aa.jpg",
      "avatar_content_type": "image/jpeg",
      "avatar_file_size": 29386,
      "avatar_updated_at": "2016-09-11T11:00:26.994Z",
      "uuid": "f22e1746-1a1f-41cb-a6dc-4e70d8c2860a"
    },
    "created_at": "2016-09-11T11:00:29.882Z",
    "updated_at": "2016-09-24T19:12:51.994Z",
    "project_name": "Softprojecq",
    "kind": "Task",
    "priority": 2
  }]
}
*/
