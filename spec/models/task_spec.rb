require 'rails_helper'

RSpec.describe Task, type: :model do
  describe "Check task validion" do
    let(:user) { FactoryGirl.build(:user)}
    let(:project) {FactoryGirl.create(:project, owner_id: user.uuid)}
    let(:sprint) {FactoryGirl.create(:sprint, project_id: project.id)}
    let(:task) {FactoryGirl.create(:task, project_id: project.id)}

    it 'has name' do
      expect(task.name).to eq('Test Task')
    end
    it 'has kind' do
      expect(task.kind).to eq('Task')
    end
    it 'has priority' do
      expect(task.priority).to eq(1)
    end
    it 'has description' do
      expect(task.description).to eq('Test task description')
    end
    it 'has status' do
      expect(task.status).to eq('Created')
    end
  end
  describe "Check task time validion" do
    let(:user) { FactoryGirl.build(:user)}
    let(:project) {FactoryGirl.create(:project, owner_id: user.uuid)}
    let(:sprint) {FactoryGirl.create(:sprint, project_id: project.id)}
    let(:task) {FactoryGirl.create(:task, project_id: project.id, estimation: '2h 40m', remaining: '2h 40m', worked: '1h 15m')}

    it 'has proper validations of estimation worked and remaining fields' do
      expect(task).to be_valid
    end
  end
  describe "Check task time validion" do
    let(:user) { FactoryGirl.create(:user)}
    let(:project) {FactoryGirl.create(:project, owner_id: user.uuid)}
    let(:sprint) {FactoryGirl.create(:sprint, project_id: project.id)}
    #let(:task) {FactoryGirl.create(:task, project_id: project.id, estimation: '2c 40m', remaining: '2h 40m', worked: '1h 15m')}

    #it 'has inproper validations of estimation field' do
    #  task.vaid?
    #  task.errors.should have_key(:estimation)
    #end
  end
end
