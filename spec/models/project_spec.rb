require 'rails_helper'

RSpec.describe Project, type: :model do
  let(:user) { FactoryGirl.build(:user) }
  let(:project) {FactoryGirl.create(:project, name: 'Test project', description: 'Test project description',
    status: 'Created', owner_id: user.uuid )}
  describe "Checking valid project" do
    it 'has name' do
      expect(project.name).to eq('Test project')
    end
    it 'has description' do
      expect(project.description).to eq('Test project description')
    end
    it 'has status Created' do
      expect(project.status).to eq('Created')
    end
    it 'has owner_id as user uuid' do
      expect(project.owner_id).to eq(user.uuid)
    end
 
  end


  describe "Checking invalid project" do
    it 'has no name' do
      project = Project.create(name: '', description: 'Test project description',
        status: 'Created', owner_id: user.uuid)
      project.valid?
      expect(project.errors).to have_key(:name)
    end
    it 'has no description' do
      project = Project.create(name: 'Test project', description: '',
        status: 'Created', owner_id: user.uuid)
      project.valid?
      project.errors.should have_key(:description)
    end
    it 'has no status' do
      project = Project.create(name: 'Test project', description: 'Test project description',
        status: '', owner_id: user.uuid)
      project.valid?
      project.errors.should have_key(:status)
    end
    it 'has no owner_id assigned' do
      project = Project.create(name: 'Test project', description: 'Test project description',
        status: 'Created', owner_id: nil)
      project.valid?
      project.errors.should have_key(:owner_id)
    end
    it 'has integer assigned to owner_id' do
      project = Project.create(name: 'Test project', description: 'Test project description',
        status: 'Created', owner_id: 1)
      project.valid?
      project.errors.should have_key(:owner_id)
    end
  end
end
