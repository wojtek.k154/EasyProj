require 'rails_helper'

RSpec.describe User, type: :model do
  let(:user) { FactoryGirl.build(:user, email: 'test@example.com', name:'John Smith', password: 'test1234',
    password_confirmation: 'test1234', avatar: File.new("#{Rails.root}/app/assets/images/avatar/ca.jpg", "r")) }

    it "require existing name" do
      expect(user.name).to eq("John Smith")
    end
    it "require existing avatar" do
      expect(user.avatar_file_name).not_to be_empty
    end
    it "require unexisting name" do
      user = User.create(email: 'test@example.com', password: 'test1234',
        password_confirmation: 'test1234', avatar: File.new("#{Rails.root}/app/assets/images/avatar/ca.jpg", "r"))
      user.valid?
      user.errors.should have_key(:name)
    end
    it "require unexisting avatar" do
      user = User.create(email: 'test@example.com', name:'John Smith', password: 'test1234',
        password_confirmation: 'test1234')
      user.valid?
      user.errors.should have_key(:avatar)
    end
end
