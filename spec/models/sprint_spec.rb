require 'rails_helper'

RSpec.describe Sprint, type: :model do
  describe "Check valid sprint" do
    let(:user) { FactoryGirl.build(:user)}
    let(:project) {FactoryGirl.create(:project, owner_id: user.uuid)}
    let(:sprint) {FactoryGirl.create(:sprint, project_id: project.id)}
    it 'has name' do
      expect(sprint.name).to eq('Test Sprint')
    end
    it 'has start date' do
      expect(sprint.start_date).to eq(Date.today)
    end
    it 'has end date' do
      expect(sprint.end_date).to eq(Date.today + 1.week)
    end
    it 'has project_id' do
      expect(sprint.project_id).to eq(project.id)
    end
  end
end
