require 'spec_helper'

describe Api::V1::UsersController, type: :controller do
  let(:user) { FactoryGirl.create(:user) }
  let(:project) { FactoryGirl.create(:project) }

  before do
    sign_in user
    project.update(newuser: user.uuid)
  end
  
  describe "GET #index" do
    it "populates an array of users" do
      get :index, { projectID: project.id }
      expect(response).to be_success
      expect(response).to have_http_status(200)
    end
    it "renders the :index json" do
      get :index, format: :json
      expect(response.header['Content-Type']).to have_content('application/json')
    end
  end

  describe "GET #show" do
    it "assigns the requested contact to @contact" do
      get :show, uuid: user.uuid
      expect(response).to be_success
    end
    it "renders the :show json" do
      get :show, uuid: user.uuid
      expect(response.header['Content-Type']).to have_content('application/json')
    end
  end
end
