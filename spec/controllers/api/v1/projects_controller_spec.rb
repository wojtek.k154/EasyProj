require 'rails_helper'

describe Api::V1::ProjectsController, type: :controller do
  let(:user) { FactoryGirl.create(:user) }
  let(:project) { FactoryGirl.create(:project) }
  before do
    sign_in user
  end

  describe "GET #index" do
    it "populates an array of projects" do
      get :index
      expect(response).to be_success
      expect(response).to have_http_status(200)
    end
    it "renders the :index json" do
      get :index
      expect(response.header['Content-Type']).to have_content('application/json')
    end
  end

  describe "GET #show" do
    it "assigns the requested project to @project" do
      get :show, id: project.id
      expect(response).to be_success
    end
    it "renders the :show json" do
      get :show, id: project.id
      expect(response.header['Content-Type']).to have_content('application/json')
    end
  end


  describe "POST #create" do
    context "with valid attributes" do
      it "saves the new project in the database" do

        expect{
          post :create, { name: "First", description: "dasdas as das as", status: "Created" , owner_id: user.uuid }
        }.to change{Project.all.reload.count }
      end
      it "render" do
          post :create, { name: "First", description: "dasdas as das as", status: "Created" , owner_id: user.uuid }
        expect(response.header['Content-Type']).to have_content('application/json')
      end
    end

    context "with invalid attributes" do
      it "does not save the new project in the database" do
        expect{
          post :create, { name: "First", description: "dasdas as das as", status: "Created" }
        }.to_not change(Project,:count)
      end

    end
  end
  describe "PUT #update" do
    context "with valid attributes" do
      it "update the project in the database" do
        put :update, {id: project.id, status: "InProgres" }
        expect(response).to be_success
      end
    end

    context "with invalid attributes" do
      it "does not update the project in the database" do
        put :update,{ id: project.id, name: nil }
        expect(response).to_not be_success
      end
    end
  end
  describe "DELETE #destroy" do
    it "delete project from the database" do
      project = Project.create( name: "First", description: "dasdas as das as", status: "Created", owner_id: user.uuid )
      expect{
        delete :destroy,{ id: project.id}
      }.to change(Project, :count).by(-1)
    end
  end
end
