require 'rails_helper'


describe Api::V1::NotificationsController, type: :controller do
  let(:user) { FactoryGirl.create(:user) }
  let(:project) { FactoryGirl.create(:project) }
  let(:task) {FactoryGirl.create(:task, project_id: project.id) }
  let(:sprint) {FactoryGirl.create(:sprint) }


  describe "GET #index" do
    it "populates an array of notifications" do
      get :index
      expect(response).to be_success
      expect(response).to have_http_status(200)
    end
    it "renders the :index json" do
      get :index
      expect(response.header['Content-Type']).to have_content('application/json')
    end
  end
  describe "POST #create" do
    context "with valid attributes" do
      it "saves the new notification in the database" do
        expect{
          post :create, { trackable_type: "Project", trackable_id: project.id, user_id: user.uuid, key: "add new sprint" }
        }.to change{Notification.all.reload.count}.by(1)
      end
      it "render" do
        post :create, { trackable_type: "Project", trackable_id: project.id, user_id: user.uuid, key: "add new sprint" }
        expect(response.header['Content-Type']).to have_content('application/json')
      end
    end
  end
end
