require 'rails_helper'

describe Api::V1::SprintsController, type: :controller do
  let(:user) { FactoryGirl.create(:user) }
  let(:project) { FactoryGirl.create(:project, owner_id: user.uuid) }
  let(:sprint) {FactoryGirl.create(:sprint, project_id: project.id) }
  before do
    sign_in user
    project.update(newuser: user.uuid)
  end
  describe "GET #index" do
    it "populates an array of sprints" do
      get :index, {project_id: project.id}
      expect(response).to be_success
      expect(response).to have_http_status(200)
    end
    it "renders the :index json" do
      get :index, {project_id: project.id}
      expect(response.header['Content-Type']).to have_content('application/json')
    end
  end

  describe "GET #show" do
    it "assigns the requested sprint to @sprint" do
      get :show,{ project_id: sprint.project_id, id: sprint.id}
      expect(response).to be_success
    end
    it "renders the :show json"do
      get :show, project_id: sprint.project_id, id: sprint.id
      expect(response.header['Content-Type']).to have_content('application/json')
    end
  end


  describe "POST #create" do
    context "with valid attributes" do
      it "saves the new sprint in the database" do
        expect{
          post :create,{ project_id: project.id, name: "Temp Sprint", start_date: Date.today, end_date: (Date.today + 1.week), status: "Created" }
        }.to change{Sprint.all.reload.count}.by(1)
      end
      it "render create json" do
        post :create,{ project_id: project.id, name: "Temp Sprint", start_date: Date.today, end_date: (Date.today + 1.week), status: "Created" }
        expect(response.header['Content-Type']).to have_content('application/json')
      end
    end

    context "with invalid attributes" do
      it "does not save the new sprint in the database" do
        expect{
          post :create,{ project_id: project.id, name: "Temp Sprint", start_date: Date.today  }
        }.to_not change(Sprint,:count)
      end
    end
  end

  describe "PUT #updatee" do
    context "with valid attributes" do
      it "update the sprint in the database" do
        put :update, { project_id: project.id, id: sprint.id, status: "InProgres" }
        expect(response).to be_success
      end
    end

    context "with invalid attributes" do
      it "does not update the sprint in the database" do
        put :update ,{ project_id: project.id, id: sprint.id, status: "" }
        expect(response).to_not be_success
      end
    end
  end
  describe "DELETE #destroy" do
    it "delete sprint from the database" do
      spr = Sprint.create!( name: "First", start_date: Date.today, end_date: Date.today + 1.week, status: "Created", project_id: project.id )
      expect{
        delete :destroy, {project_id: project.id, id: spr.id}
      }.to change{Sprint.all.reload.count}.by(-1)
    end
    it "render"do
      expect(response).to be_success
    end
  end
end
