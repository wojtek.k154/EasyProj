require 'rails_helper'

describe Api::V1::TasksController, type: :controller do
  let(:user) { FactoryGirl.create(:user) }
  let(:project) { FactoryGirl.create(:project, owner_id: user.uuid) }
  let(:task) {FactoryGirl.create(:task, project_id: project.id) }
  before do
    sign_in user
    project.update(newuser: user.uuid)
  end
  describe "GET #index" do
    it "populates an array of tasks" do
      get :index,{ project_id: project.id }
      expect(response).to be_success
    end
    it "renders the :index json" do
      get :index,{ project_id: project.id }
      expect(response.header['Content-Type']).to have_content('application/json')
    end
  end

  describe "GET #show" do
    it "assigns the requested task to @task" do
      get :show, { project_id: task.project_id, id: task.id }
      expect(response).to be_success
    end
    it "renders the :show json" do
      get :show, { project_id: task.project_id, id: task.id }
      expect(response.header['Content-Type']).to have_content('application/json')
    end
  end


  describe "POST #create" do
    context "with valid attributes" do
      it "saves the new task in the database" do
        expect{
          post :create, task: { kind: 'Task', priority: 1, name: 'Test Task',
            description: 'Test task description', status: 'Created', estimation: '4h 45m' }, project_id: project.id
        }.to change{Task.all.reload.count}.by(1)
      end
      it "render" do
        post :create, task: {kind: 'Task', priority: 1, name: 'Test Task',
          description: 'Test task description', status: 'Created', estimation: '4h 45m'}, project_id: project.id
        expect(response.header['Content-Type']).to have_content('application/json')
      end
    end

    context "with invalid attributes" do
      it "does not save the new task in the database" do
        expect{
          post :create, task: { kind: 'Task', priority: 1, name: 'Test Task',
            description: 'Test task description', status: 'Created', estimation: '2@!qwe' }, project_id: project.id
        }.to_not change(Task, :count)
      end
    end
  end
  describe "PUT #update" do
    context "with valid attributes" do
      it "update the task in the database" do
        put :update, task: { worked: "2h" }, project_id: project.id, id: task.id
        expect(response).to be_success
      end
    end

    context "with invalid attributes" do
      it "does not update the task in the database" do
        put :update, task: { priority: nil }, project_id: project.id, id: task.id
        expect(response).to_not be_success
      end
    end
  end
  describe "DELETE #destroy" do
    it "delete task from the database" do
      task = Task.create!(name: "First", description: "First description", kind: "Task", priority: 1, project_id: project.id )
      expect{
        delete :destroy, { project_id: task.project_id, id: task.id }
      }.to change{ Task.all.reload.count }.by(-1)
    end
  end

end
