require 'spec_helper'
Capybara.default_wait_time = 10
feature "Checking projects list", :js => true do
  let(:user) { FactoryGirl.build(:user) }
  let(:project) { FactoryGirl.build(:project) }
  before(:all) do
    @user2 = User.create(email: 'test1@example.com', name: 'Test Test1', password: 'test1234', password_confirmation: 'test1234', avatar: File.new("#{Rails.root}/app/assets/images/avatar/ca.jpg", "r"))
    User.create!(email: 'test2@example.com', name: 'Test2 Test2', password: 'test1234', password_confirmation: 'test1234', avatar: File.new("#{Rails.root}/app/assets/images/avatar/aa.jpg", "r"))

  end
  before(:each) do
    visit '/dashboard/projects'
  end

  after(:all) do
    Project.destroy_all
    Task.destroy_all
    User.destroy_all
    UserProject.destroy_all
  end

  scenario "show project's list" do
    sleep 5
    visit '/login'
    sign_in_with(@user2.email, @user2.password)
    within("div.panel-heading") do
      expect(page).to have_content("Project's list")
    end
    #log_out
  end

  scenario "create new project" do
    sleep 5
    visit '/login'
    sign_in_with(@user2.email, @user2.password)
    create_project(project)
    save_and_open_page
    sleep 7
    within("ol.breadcrumb") do
      within(".active") do
        save_and_open_page
        expect(page).to have_content("Project's View - Sprints")
      end
    end
    #log_out
  end

  scenario "edit project" do
    sleep 5
    visit '/login'
    save_and_open_page
    sign_in_with(@user2.email, @user2.password)
    save_and_open_page

    within("table", wait: 10) do
      within("tbody") do
        within("tr") do
          find(".btn-info").find(".fa-edit").click
        end
      end
    end
    within("div#myModal1") do
      within("div.modal-body") do
        fill_in "Name", with: "Updated Project"
        fill_in "Description", with: project.description
      end
      within("div.modal-footer") do
        find(".btn-success").click
      end
    end
    within("table") do
      within("tbody") do
        within("tr") do
          expect(page).to have_content("Updated Project")
        end
      end
    end
  end

  scenario "delete project" do
    sleep 5
    visit '/login'
    save_and_open_page
    sign_in_with(@user2.email, @user2.password)
    within("table", wait: 10) do
      within("tbody") do
        within("tr") do
          find(".btn-danger").find(".fa-trash").click
        end
      end
    end

    within("table", wait: 3) do
      within("tbody") do
        expect(page).to_not have_content("Updated Project")
      end
    end
  end

  def create_project(project)
    within("div.panel-heading", wait: 10) do
      find(".pull-right").find(".btn-success").find(".fa-plus").click
    end
    within("div#myModal") do
      within("div.modal-body") do
        fill_in "Name", with: project.name
        fill_in "Description", with: project.description
      end
      within("div.modal-footer") do
        find(".btn-success").find(".fa-plus").click
      end
    end
  end


  def sign_in_with(email, password)
    within("fieldset", wait: 3) do
      fill_in 'email', with: email
      fill_in 'Password', with: password
    end
    click_button "LogIn"
  end

  def log_out
    within("ul.nav.navbar-top-links.navbar-right") do
      find(".dropdown").find(".dropdown-toggle").click
      within(".dropdown-menu.dropdown-user") do
        find(".fa-sign-out").click
      end
    end
  end
end
