require 'spec_helper'

feature "Project's sprints list", :js => true do
  let(:user) { FactoryGirl.build(:user) }
  let(:project) { FactoryGirl.build(:project) }
  let(:sprint) { FactoryGirl.build(:sprint) }
  before(:all) do
    @user2 = User.create(email: 'test1@example.com', name: 'Test Test1', password: 'test1234', password_confirmation: 'test1234', avatar: File.new("#{Rails.root}/app/assets/images/avatar/ca.jpg", "r"))
  end
  before(:each) do
    visit '/dashboard/projects'
  end

  after(:all) do
    Project.destroy_all
    Task.destroy_all
    User.destroy_all
    UserProject.destroy_all
  end

  scenario "show sprint list" do
    sleep 5
    visit '/login'
    sign_in_with(@user2.email, @user2.password)
    create_project(project)
    within("ol.breadcrumb") do
      within("li.active") do
        expect(page).to have_content("Project's View - Sprints")
      end
    end
  end

  scenario "add new sprint" do
    sleep 5
    visit '/login'
    sign_in_with(@user2.email, @user2.password)
    within(".panel-body") do
      within("tbody") do
        within("tr") do
          find(".btn-success").find(".fa-check").trigger('click')
        end
      end
    end
    within("div.panel-default") do
      within("div.panel-heading") do
        within("span.pull-right") do
          find(".btn-success").find(".fa-plus").trigger('click')
        end
      end
    end
    create_sprint(sprint)
    within("div.panel-default") do
      within("div.panel-body") do
        within("table")  do
          within("tbody") do
            expect(page).to have_content(sprint.name)
          end
        end
      end
    end
  end

  scenario "update sprint" do
    sleep 5
    visit '/login'
    sign_in_with(@user2.email, @user2.password)
    within(".panel-body") do
      within("table", :focus) do
        within("tbody") do
          within("tr") do
            find(".btn-success").find(".fa-check").trigger('click')
          end
        end
      end
    end
    within("div.panel-default") do
      within(".panel-body") do
        within("table") do
          within("tbody") do
            within("tr") do
              find(".btn-info").find(".fa-pencil-square-o").trigger('click')
            end
          end
        end
      end
    end
    within("div#myModal1") do
      within("div.modal-body") do
        fill_in "Name", with: project.name
        find("#status").find(:xpath, 'option[2]').select_option
      end
      within("div.modal-footer") do
        find(".btn-success").trigger('click')
      end
    end

  end

  def create_sprint(sprint)
    within("div#myModal") do
      within("div.modal-body") do
        fill_in "Name", with: sprint.name
        fill_in "Start date", with: sprint.start_date
        fill_in "End date", with: sprint.end_date
      end
      within("div.modal-footer") do
        find(".btn-success").find(".fa-plus").trigger('click')
      end
    end
  end

  def create_project(project)
    within(".panel-heading") do
      within(".pull-right") do
        find(".btn-success").find(".fa-plus").trigger('click')
      end
    end

    within("div#myModal") do
      within("div.modal-body") do
        fill_in "Name", with: project.name
        fill_in "Description", with: project.description
      end
      within("div.modal-footer") do
        find(".btn-success").find(".fa-plus").trigger('click')
      end
    end
  end

  def sign_in_with(email, password)
    within("fieldset") do
      fill_in 'email', with: email
      fill_in 'Password', with: password
    end
    click_button "LogIn"
  end
end
