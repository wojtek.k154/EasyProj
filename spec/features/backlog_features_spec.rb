# frozen_string_literal: true
require 'spec_helper'

feature "tasks in project's backlog", js: true do
  let(:task) { FactoryGirl.build(:task) }

  before(:all) do
    @user2 = User.create(email: 'test1@example.com', name: 'Test Test1', password: 'test1234', password_confirmation: 'test1234', avatar: File.new("#{Rails.root}/app/assets/images/avatar/ca.jpg", 'r'))
    @project = Project.create(name: 'First project', description: 'First project description', owner_id: @user2.uuid, status: 'Created')
    @project.user_projects.create!(user_id: @user2.uuid)
  end

  before(:each) do
    visit '/dashboard/projects'
  end
  after(:all) do
    Project.destroy_all
    Task.destroy_all
    User.destroy_all
    UserProject.destroy_all
  end

  scenario 'add new task to backlog', js: true do
    sleep 5
    backlog
    sleep 6
    within('.panel-default') do
      within('.panel-heading') do
        find('.btn-success').find('.fa-plus').trigger('click')
      end
    end
    save_and_open_page
    task.estimation = '2h 45m'
    task_fill(task)
    within ('div.modal-footer') do
      find('.btn-success').find('.fa-plus').trigger('click')
    end
    save_and_open_page
    within('.panel-body') do
      expect(page).to have_content(task.name)
    end
  end

  scenario 'update existing task', js: true do
    sleep 5
    backlog
    sleep 5
    save_and_open_page
    within('.panel-default') do
      within('.panel-body') do
        find('.btn-info').find('.fa-pencil-square-o').trigger('click')
      end
    end

    task.name = 'upadted task name'
    task_fill(task)
    within ('div.modal-footer') do
      find('.btn-success').trigger('click')
    end
    within('.panel-body') do
      expect(page).to have_content('upadted task name Type: Task Priority: 1 Estimante:')
    end
  end

  scenario 'delete task', js: true do
    sleep 5
    backlog
    sleep 7
    save_and_open_page
    within('.panel-default') do
      within('.panel-body') do
        find('.btn-danger').find('.fa-trash').trigger('click')
      end
    end
    within('.panel-body') do
      expect(page).to_not have_content('upadted task name Type: Task Priority: 1 Estimante:')
    end
  end

  def sign_in_with(email, password)
    fill_in 'email', with: email
    fill_in 'Password', with: password
    click_button 'LogIn'
  end

  def backlog
    visit "/login"
    sign_in_with(@user2.email, @user2.password)
    sleep 7
    save_and_open_page
    within('.panel-default') do
      within('.panel-body') do
        within('tbody') do
          within('tr') do
            find('.btn-success').find('.fa-check').click
          end
        end
      end
    end
    within('ul.nav-pills.nav-justified') do
      click_link 'Tasks'
    end
 end

  def task_fill(task)
    within('div.modal-body') do
      find(:xpath, '//*[@id ="dropdownMenu1"]').trigger('click')
      find(:xpath, '//*[@id ="kind"]/li[1]/a').trigger('click')
      find(:xpath, '//*[@id ="dropdownMenu2"]').trigger('click')
      find(:xpath, '//*[@id ="priority"]/li[1]/a').trigger('click')
      fill_in 'Name', with: task.name
      fill_in 'Description', with: task.description
      fill_in 'Estimation', with: task.estimation
      click_button 'Assign to Me'
    end
  end
end
