require 'spec_helper'

class LoginUserForm
  include Capybara::DSL

  def login(user)
    visit '/login'
    within("fieldset") do
      fill_in 'email', with: user.email
      fill_in 'Password', with: user.password
    end
    click_button "LogIn"
  end

end
