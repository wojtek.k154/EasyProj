require 'spec_helper'

feature "Project's users list", :js => true do
  before(:all) do
    @project = FactoryGirl.build(:project)
    @user2 = User.create(email: 'test1@example.com', name: 'Test1 Test1', password: 'test1234', password_confirmation: 'test1234', avatar: File.new("#{Rails.root}/app/assets/images/avatar/ca.jpg", "r"))
    User.create(email: 'test12@example.com', name: 'Test2 Test2', password: 'test1234', password_confirmation: 'test1234', avatar: File.new("#{Rails.root}/app/assets/images/avatar/aa.jpg", "r"))
    @project = Project.create(name: @project.name, description: @project.description, owner_id: @user2.uuid)
    @project.user_projects.create!(user: @user2)
  end
  before(:each) do
    visit '/dashboard/projects'
  end
  after(:all) do
    Project.destroy_all
    Task.destroy_all
    User.destroy_all
    UserProject.destroy_all
  end

  scenario "drag user to project's list" do
    sleep 5
    visit '/login'
    user_list
    element = page.find(:xpath, '//*[@id="unsigned"]/div[2]/ul/li[1]')
    target = page.find(:xpath, '//*[@id="signed"]/div[2]/ul')
    element.drag_to(target)
    save_and_open_page
    expect(page).to have_content("Test2 Test2")
  end

  scenario "drag user from project's list" do
    sleep 5
    visit '/login'
    user_list
    element = page.find(:xpath, '//*[@id="signed"]/div[2]/ul/li[1]')
    target = page.find(:xpath, '//*[@id="unsigned"]/div[2]/ul')
    element.drag_to(target)
    save_and_open_page
    expect(page).to have_content("Test2 Test2")
  end


  def sign_in_with(email, password)
    within("fieldset") do
      fill_in 'email', with: email
      fill_in 'Password', with: password
    end
    click_button "LogIn"
  end
  
  def user_list
    sign_in_with(@user2.email, @user2.password)
    within(".panel-default") do
      within(".panel-body") do
        within("tbody") do
          within("tr") do
            find(".btn-success").find(".fa-check").trigger('click')
          end
        end
      end
    end
    within("ul.nav-pills.nav-justified") do
      click_link "Users"
    end
  end
end
