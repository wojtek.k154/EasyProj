require 'spec_helper'

feature "Log in and Sign up to application", :js => true do
  let(:user) {FactoryGirl.build(:user)}
  before(:all) do
    @user2 = User.create(email: 'test1@example.com', name: 'Test Test1', password: 'test1234',
                         password_confirmation: 'test1234',
                         avatar: File.new("#{Rails.root}/app/assets/images/avatar/ca.jpg", "r"))
  end
  before(:each) do
    visit '/dashboard/projects'
  end

  after(:all) do
    Capybara.use_default_driver
    Project.destroy_all
    Task.destroy_all
    User.destroy_all
    UserProject.destroy_all
  end

  scenario "check Login form if user logged out", :js => true do
    expect(page).to have_content('Easy Project')
  end

  scenario "check signup form if user logged out", :js => true do
    visit "/signup"
    within("div.panel-heading") do
      expect(page).to have_content('Please Sign Up')
    end
  end

  scenario "sign up with invalid data", :js => true do
    user1 = user
    user1.email = "test"
    visit "/signup"
    sign_up_with(user1)
    within("div.panel-heading") do
      expect(page).to have_content('Please Sign Up')
    end
  end

  scenario "sign up with valid data", :js => true do
    visit "/signup"
    fill_in 'email', with: 'wojtew123@wp.pl'
    fill_in 'name', with: 'test1234a'
    fill_in 'password', with: 'test1234'
    fill_in 'password_confirmation', with: 'test1234' 
    page.find(:xpath, "//*[@id='avatar']", visible: false).set File.new("#{Rails.root}/app/assets/images/avatar/ca.jpg", 'r')
    #page.attach_file('avatar', "#{Rails.root}/app/assets/images/avatar/ca.jpg" )
    click_button "Register"
      sleep 7
      expect(page).to have_content("Project\'s list")
    #end
  end


  scenario "login with valid email and password", :js => true do
    visit '/login'
    sign_in_with(@user2.email, @user2.password)
    within("div.panel-heading") do
      expect(page).to have_content("Project\'s list")
    end
  end

  scenario "login with invalid email and password", :js => true do
    visit '/login'
    sign_in_with("user_email", @user2.password)
    within("div.panel-heading") do
      expect(page).to have_content("Please Sign In")
    end
  end

  def sign_up_with(user)
    #within("fieldset") do
      page.find(:xpath, '//div[1]/input').set user.email
      fill_in 'name', with: user.name
      fill_in 'Password', with: user.password
      fill_in 'Password Confirmation', with: user.password
      #page.find(:xpath, '//*[@id="avatar"]').set File.new("#{Rails.root}/app/assets/images/avatar/ca.jpg", "r")
      click_button "Register"
    #end
  end
  def sign_in_with(email, password)
    within("fieldset") do
      fill_in 'email', with: email
      fill_in 'Password', with: password
    end
    click_button "LogIn"
  end
end
