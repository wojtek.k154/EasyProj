require 'spec_helper'

feature "Project's tasks list", :js => true do
  let(:task) { FactoryGirl.build(:task)}
  before(:all) do
    Capybara.current_driver = :selenium
    @project = FactoryGirl.build(:project)
    @user2 = User.create(email: 'test1@example.com', name: 'Test Test1', password: 'test1234', password_confirmation: 'test1234', avatar: File.new("#{Rails.root}/app/assets/images/avatar/ca.jpg", "r"))
    @project = Project.create(name: @project.name, description: @project.description, owner_id: @user2.uuid)
    @project.user_projects.create!(user: @user2)
  end
  before(:each) do
    visit '/dashboard/projects'
  end
  after(:all) do
    Capybara.use_default_driver
    Project.destroy_all
    Task.destroy_all
    User.destroy_all
    UserProject.destroy_all
  end

  scenario "estimate task" do
    sleep 5
    visit '/login'
    backlog
    add_task
    click_button "Estimate"
    within("#myModal1") do
      within(".modal-body") do
        fill_in "Estimate", with: "2h 12m"
      end
      within(".modal-footer") do
        click_button "Estimate"
      end
    end
    sleep 2
  end

  scenario "log work" do
    sleep 5
    visit '/login'
    backlog
    within("div.panel-body") do
      click_link task.name
    end
    click_button "Log Work"
    within("#myModal2") do
      within(".modal-body") do
      fill_in "Logwork", with: "2h 12m"
      end
      within(".modal-footer") do
        click_button "Log work"
      end
    end

  end

  scenario "set status task" do
    sleep 5
    visit '/login'
    backlog
    within("div.panel-body") do
      click_link task.name
    end
    click_button "Set Status"
    within("#myModal3") do
      within(".modal-body") do
        find("#dropdownMenu2").trigger('click')
        find(:xpath, '//*[@id="status"]/li[2]/a').trigger('click')
      end
      within(".modal-footer") do
        click_button "Set Status"
      end
    end
    sleep 2
  end


  def sign_in_with(email, password)
    within("fieldset") do
      fill_in 'email', with: email
      fill_in 'Password', with: password
    end
    click_button "LogIn"
  end
  def backlog
    sign_in_with(@user2.email, @user2.password)
    within(".panel-default") do
      within(".panel-body") do
        within("table") do
          within("tbody") do
            within("tr") do
              find(".btn-success").find(".fa-check").trigger('click')
            end
          end
        end
      end
    end
    within("ul.nav-pills.nav-justified") do
      click_link "Tasks"
    end
  end

  def add_task
    within(".panel-default") do
      within(".panel-heading") do
        find(".btn-success").find(".fa-plus").trigger('click')
      end
    end
    task.estimation = "2h 45m"
    task_fill(task)
    within ("div.modal-footer") do
      find(".btn-success").find(".fa-plus").trigger('click')
    end
    within("div.panel-body") do
      click_link task.name
    end
  end

  def task_fill(task)
    within("div.modal-body") do
      find(:xpath, '//*[@id ="dropdownMenu1"]').trigger('click')
      find(:xpath, '//*[@id ="kind"]/li[1]/a', ).trigger('click')
      find(:xpath, '//*[@id ="dropdownMenu2"]').trigger('click')
      find(:xpath, '//*[@id ="priority"]/li[1]/a', ).trigger('click')
      fill_in "Name", with: task.name
      fill_in "Description", with: task.description
      fill_in "Estimation", with: task.estimation
      click_button "Assign to Me"
    end
  end
end
